//
// Created by flosc on 25.05.2020.
//

#ifndef LECRP_CRDIFFUSION_HPP
#define LECRP_CRDIFFUSION_HPP

#include "NDVector.hpp"
#include "plot.hpp"

/**
 * Solve the diffusion equation for cosmic rays (CR) numerical: <BR>
 * du/dt - kappa Laplace(u) + d/dp (b * u) Heaviside(z &le h) = 0 <BR>
 * u(r=R) = 0, u(z=-L) = u(r=L) = 0 <BR>
 * u(t=0) = DiracDelta(r) q <BR>
 * kappa = p^delta, b = -p^n, q = p^-gamma <BR>
 * u = u[log_p, z, r]
 * @relatesalso NDVector, NDNumerics
 */
namespace CRDiff {

    /**
     * Calculate the energy loss rate of real CR diffusion for the given log_p. <BR>
     * dp/dt = dp/dE dE/dt
     * @param log_p the log_p to calculate the energy loss rate
     * @return energy loss rate at log_p
     * @note The function u is scaled with exp(log_p), so the energy loss rate is scaled with exp(-log_p)
     */
    NumType energy_loss_rate(NumType_I &log_p);

    /**
     * Calculate the diffusion part of the equation for one time step for a non-equidistant grid. <BR>
     * du/dt = kappa(p) * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) <BR>
     * kappa(p) = p^delta <BR>
     * Uses only positive z grid.
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dr, dz Vector of step sizes
     * @param[in] dt step size
     * @param[in] delta delta in kappa = p^delta
     * @param[in] r, log_p variables
     */
    void diffusion_step_r_z(VecND &un, VecNum_I &dr, VecNum_I &dz, NumType_I &dt, NumType_I &delta,
                            VecNum_I &r, VecNum_I &log_p);

    /**
     * Calculate the diffusion part of the equation for one time step for a non-equidistant grid, using the "parameter_values". <BR>
     * du/dt = kappa(p) * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) <BR>
     * kappa(p) = beta (1 + p/m)^delta <BR>
     * Uses only positive z grid.
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dr, dz Vector of step sizes
     * @param[in] dt step size
     * @param[in] delta delta in kappa
     * @param[in] r, log_p variables
     */
    void diffusion_step_r_z_2(VecND &un, VecNum_I &dr, VecNum_I &dz, NumType_I &dt, NumType_I &delta,
                              VecNum_I &r, VecNum_I &log_p);

    /**
     * Calculate the energy loss part of the equation for one time step. <BR>
     * du/dt = - d/dp(b(p) u) = 2 exp(log_p) u + exp(log_p) du/dlog_p
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dlog_p, dt step size
     * @param[in] power_n n in b = -p^n
     * @param[in] log_p variable
     * @param[in] end_z stop index for energy loss in z
     */
    void calculate_step_p(VecND &un, NumType_I &dlog_p, NumType_I &dt, NumType_I &power_n, VecNum_I &log_p, SizeType_I &end_z);

    /**
     * Calculate the energy loss part of the equation for one time step, using the "parameter_values". <BR>
     * du/dt = dE/dt du/dp = exp(-log_p) dE/dt du/dlog_p
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dlog_p, dt step size
     * @param[in] log_p variable
     * @param[in] end_z stop index for energy loss in z
     */
    void calculate_step_p_2(VecND &un, NumType_I &dlog_p, NumType_I &dt, VecNum_I &log_p, SizeType_I &end_z);

    /**
     * Calculate u(t=0) = dirac_delta(r) q(p) = 1/(2 pi r) dirac_delta(r) dirac_delta(z) q(p). <BR>
     * u is scaled with exp(log_p). <BR>
     * In comparison with the greens function to the diffusion equation dirac_delta(r) is written as: <BR>
     * dirac_delta(r) = 1 / sqrt(2 pi epsilon) dirac_delta(r, epsilon) dirac_delta(z, epsilon) <BR>
     * with dirac_delta(x, epsilon) = 1 / sqrt(2 pi epsilon) exp(-x^2 / (2 epsilon)) <BR>
     * with epsilon = 2 * dt * offset
     * @param[in, out] un the function u to be set to u0.
     * @param[in] gamma gamma in q = p^-gamma
     * @param[in] r, z, log_p variables
     * @param[in] epsilon epsilon = 2 * dt * offset
     */
    void calculate_u0(VecND &un, NumType_I &gamma, VecNum_I &r, VecNum_I &z, VecNum_I &log_p, NumType_I &epsilon);

    /**
     * Calculate u(t=0) = q(p) for r < r_min, z < z_min, u=0 outside. <BR>
     * This is the same initial condition as used by Minh.
     * u is scaled with exp(log_p).
     * @param[in, out] un the function u to be set to u0.
     * @param[in] gamma gamma in q = p^-gamma
     * @param[in] r, z, log_p variables (only positive z grid!)
     * @param[in] r_min, z_min size of the cylinder
     */
    void calculate_u0_minh(VecND &un, NumType_I &gamma, VecNum_I &r, VecNum_I &z, VecNum_I &log_p,
                           NumType_I &r_min, NumType_I &z_min);

    /**
     * Plots and print out the energy loss rate.
     * @param dlog_p step size
     * @param log_p_start, log_p_end start/end of log_p
     * @param dt step size, to calculate whether the chosen parameters are stable
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void plot_energy_loss_rate(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &dt = 0,
                               const std::string &file_appendix = "");

    /**
     * Plots and print out the diffusion coefficient.
     * @param dlog_p step size
     * @param log_p_start, log_p_end start/end of log_p
     * @param delta delta in kappa
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void plot_diffusion_coefficient(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &delta,
                                    const std::string &file_appendix = "");

    /**
     * Calculate the template of the green's function of CRDiffusion and saves it as .hdf5.
     * Uses a non-equidistant grid in r and z. The gor in log_p must be equidistant. z grid is only positive.
     * @param r, z, log_p, t Grids of the variables. Only the grid in log_p must be equidistant.
     * @param steps NumVector of the different time steps that should be saved, is casted into int
     * @param h thickness of the disk with energy loss
     * @param delta delta in kappa = p^delta
     * @param power_n n in b = -p^n
     * @param gamma gamma in q = p^-gamma
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void generate_template_varying_grid(VecNum_I &r, VecNum_I &z, VecNum_I &log_p, VecNum_I &t, VecNum_I &steps,
                                        NumType_I &h, NumType_I &delta, NumType_I &power_n, NumType_I &gamma,
                                        const std::string &file_appendix = "");
}

#endif //LECRP_CRDIFFUSION_HPP
