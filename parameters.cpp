//
// Created by flosc on 03.06.2020.
//
#include "parameters.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

using namespace std;

void parameter::load_parameters() {
    double log10_Emin, log10_Emax, dlogE, dlogE_scale;
    double log10_zmin, log10_zmax, dlogz, log10_h;
    double log10_rmin, log10_rmax, dlogr;
    double log10_tmin, log10_tmax, dlogt, dlogt_scale;
    double sourceE, vA;

    ifstream input;
    input.open("parameters.dat");

    input >> log10_Emin >> log10_Emax >> dlogE >> dlogE_scale;
    input >> log10_zmin >> log10_zmax >> dlogz >> log10_h;
    input >> log10_rmin >> log10_rmax >> dlogr;
    input >> log10_tmin >> log10_tmax >> dlogt >> dlogt_scale;
    input >> sourceE >> vA;

    cout << log10_Emax << endl;
}