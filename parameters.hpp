//
// Created by flosc on 03.06.2020.
//

#ifndef LECRP_PARAMETERS_HPP
#define LECRP_PARAMETERS_HPP

#include <cmath>

/**
 * Define the chosen Parameters as Macros
 */

///Protonmass
#define MASS_P 938.272088 ///<m_p in MeV

///Parameters for real CR diffusion
#define P0 MASS_P ///<p0 in MeV, make p dimensionless
#define B0 1.96879 ///<b0 in Mev/Myr, only for normalization
#define KAPPA0 0.035 ///<kappa0 in kpc^2/Myr
#define DELTA 0.63 ///<delta in kappa
#define POWER_N -2.0 ///<n in b (not needed!)
#define GAMMA 2.2 ///<gamma in q

///Parameters for making dimensionless
#define TAU (P0 / B0) ///<tau in Myr, make t dimensionless
#define LAMBDA std::sqrt(KAPPA0 * P0 / B0) ///<lambda in kpc, make r dimensionless

///Dimensionless parameters
#define R (20.0 / LAMBDA) ///<R dimensionless (20 kpc)
#define L (4.0 / LAMBDA) ///<L dimensionless (4 kpc)
#define H (0.1 / LAMBDA) ///<h dimensionless (0.1 kpc)

///Parameters for calculation, dimensionless
#define T_START (1e-3 / TAU) ///<t_start dimensionless (1e-3 Myr)
#define T_END (1e2 / TAU) ///<t_end dimensionless (1e2 Myr)
#define DLOG_T 0.05 ///<dlog_t dimensionless

#define LOG_P_START std::log(35 / P0) ///<log_p_start dimensionless (35 MeV) (Ekin ca. 1 MeV)
#define LOG_P_END std::log(15000.0 / P0) ///<log_p_end dimensionless (15 GeV) (Ekin ca. 10GeV)

#define P_START (35 / P0) ///<p_start dimensionless (35 MeV) (Ekin ca. 1 MeV)
#define P_END (15000.0 / P0) ///<p_end dimensionless (15 GeV) (Ekin ca. 10GeV)
#define DLOG_P 0.05 ///<dlog_p dimensionless
//#define DLOG_P 0.2 ///<dlog_p old value (give ca. 12 values in a interval [p, 10p])

#define DZ (0.001 / LAMBDA) ///<dz_min
#define DR (0.001 / LAMBDA) ///<dr_min
#define DT_MIN (1e-4 / TAU) ///<dt_min
#define DT_MAX (5e-3 / TAU) ///<dt_max maximal value for chosen dlog_p
//#define DT_MAX (4.9e-3 / TAU) ///<d_max maximal value for chosen dlog_p


///Reads the parameters from paramters.dat
namespace parameter {
    void load_parameters();
}

#endif //LECRP_PARAMETERS_HPP