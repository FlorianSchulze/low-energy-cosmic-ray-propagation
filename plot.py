import sys
import matplotlib.pyplot as plt
import numpy as np


def load_csv_file(file):
    """Load the data of the NDVector in the .csv file 'file' into an numpy array"""
    size = ()
    with open(file) as f:
        size_str = f.readline()[:-2].split(';')
        for s in size_str:
            size += (int(s),)
    array = np.loadtxt(file, skiprows=1)
    array = np.reshape(array, size)
    return array


def plot_csv_file(file):
    """Plots the data in 'file' (created by save_as_csv in plot.cpp) in the currently open figure."""
    try:
        x, y = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
    except ValueError:
        return -1  # Empty file
    except IOError:
        return -1  # File dont exit
    label = 'None'
    axes_label = ['None', 'None']
    with open(file) as f:
        label = f.readline()[:-1]
        axes_label = f.readline()[:-1].split('; ')

    if axes_label[0] != 'None':
        plt.xlabel(axes_label[0])
    if axes_label[1] != 'None':
        plt.ylabel(axes_label[1])
    if label == 'None':
        label = file.rsplit('.', 1)[0]
    if label[0] == '_':
        label = label[1:]
    plt.plot(x, y, label=label)
    plt.title(label)
    return 0


def main(argv):
    # Help
    if '?' in argv or '/help' in argv:
        s_help = 'Help for ' + argv[0] + ':\n'
        s_help += argv[0] + ' [files] [kwargs] [? or /help]\n'
        s_help += 'Makes a the plot of the data saved in [files].\n'
        s_help += '\tfiles:\tA list of the files that should be plotted. Delimited by a space, at least one.\n'
        s_help += '\t\tIf more then one file is give, all the data is plotted into one graph.\n'
        s_help += '\tkwargs:\tA list of named arguments (like kwargs in python).\n'
        s_help += '\t\tsavefile:\tfile where the plot is saved\n'
        s_help += '\t\ttitle:\t\ttitle of the plot\n'
        s_help += '\t\txscale, yscale:\tscale of the axis\n'
        s_help += '\t\tsortfiles:\tsort the files alphabetical\n'
        s_help += '\t?:\tPrint out this help message. Alternative: /help\n'
        s_help += 'Example:\n'
        s_help += argv[0] + ' Data.csv savefile=nice_plot.pdf'
        print(s_help)
        sys.exit(0)

    # Sorting the arguments
    files = []
    kwargs = {}
    for arg in argv[1:]:
        if '=' in arg:
            kwargs[arg.split('=', 1)[0]] = arg.split('=', 1)[1]
        else:
            files.append(arg)
    if len(files) == 0 or files == ['*.csv']:
        print('No file to plot!')
        sys.exit(-1)

    # Standart arguments
    if 'sortfiles' not in kwargs or kwargs['sortfiles'] == 'True':
        files.sort()
    if 'savefile' not in kwargs:
        kwargs['savefile'] = files[0].rsplit('.', 1)[0] + '.pdf'
    # print(files, kwargs)

    # Plot
    fig = plt.figure()
    for file in files:
        plot_csv_file(file)
    if 'title' in kwargs:
        plt.title(kwargs['title'])
    if len(files) > 1:
        plt.legend()

    if 'xscale' in kwargs:
        plt.xscale(kwargs['xscale'])
    if 'yscale' in kwargs:
        plt.yscale(kwargs['yscale'])

    # Save the plot
    plt.savefig(kwargs['savefile'])
    plt.show()
    return 0


if __name__ == '__main__':
    exit_value = main(sys.argv)
    sys.exit(exit_value)
