//
// Created by Flo on 01.05.2020.
//

#include "tridiag.hpp"

void tridag(VecNum_I &a, VecNum_I &b, VecNum_I &c, VecNum_I &r, VecNum &u) {
    int j, n = a.size();
    NumType bet;
    VecNum gam(n);
    if (b[0] == 0.0) THROW("Error 1 in tridag");
    u[0] = r[0] / (bet = b[0]); ///forward substitution
    for (j = 1; j < n; j++) {
        gam[j] = c[j - 1] / bet;
        bet = b[j] - a[j] * gam[j];
        if (bet == 0.0) THROW("Error 2 in tridag");
        u[j] = (r[j] - a[j] * u[j - 1]) / bet;
    }
    for (j = (n - 2); j >= 0; j--) { ///backward substitution
        u[j] -= gam[j + 1] * u[j + 1];
    }
}

void cyclic(VecNum_I &a, VecNum_I &b, VecNum_I &c, NumType_I &alpha, NumType_I &beta, VecNum_I &r, VecNum &x) {
    int i, n = a.size();
    NumType fact, gamma;
    if (n <= 2) THROW("n too small for cyclic");
    VecNum bb(n), u(n), z(n);
    gamma = -b[0]; ///Avoid subtraction error in forming bb[0]
    bb[0] = b[0] - gamma; ///Set up the diagonal of the modified tridiagonal system
    bb[n - 1] = b[n - 1] - alpha * beta / gamma;
    for (i = 1; i < n - 1; i++) bb[i] = b[i];
    tridag(a, bb, c, r, x); ///Solve A*x=r
    u[0] = gamma; ///Set up the vector u
    u[n - 1] = alpha;
    for (i = 1; i < n - 1; i++) u[i] = 0.0;
    tridag(a, bb, c, u, z); ///Solve A*z=u
    fact = (x[0] + beta * x[n - 1] / gamma) / (1.0 + z[0] + beta * z[n - 1] / gamma); ///Form v*x/(1+v*z)
    for (i = 0; i < n; i++) x[i] -= fact * z[i]; ///Now get the solution vector x
}
