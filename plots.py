"""Create some standard test plots for the program."""
import plot
import CRDiffusion
import matplotlib.pyplot as plt
import numpy as np
import glob
import h5py
import time


def OneDimVarDiff(index=1, plot_error=True):
    file_list = glob.glob('OneDimVarDiff_*_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = '1D Diffusion Varying Gird'
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('OneDimVarDiff_%i.pdf' % index)

    if plot_error:
        file_list = glob.glob('OneDimVarDiff_*_%i_error.csv' % index)
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = '1D Diffusion Varying Grid Error'
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.ylim(bottom=1e-8, top=1e-2)
        plt.savefig('OneDimVarDiff_%i_error.pdf' % index)
    return 0


def OneDimVarDiffR(index=1, plot_error=True):
    file_list = glob.glob('OneDimVarDiffR_*_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = '1D Diffusion Varying Grid R coordinate'
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('OneDimVarDiffR_%i.pdf' % index)

    if plot_error:
        file_list = glob.glob('OneDimVarDiffR_*_%i_error.csv' % index)
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = '1D Diffusion Error Varying Grid'
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.ylim(bottom=1e-8, top=1e-1)
        plt.savefig('OneDimVarDiffR_%i_error.pdf' % index)
    return 0


def OneDimVarError(method='steps', variant=''):
    file_list = glob.glob('OneDimVarDiff%sError_%s_*.csv' % (variant, method))
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Error of 1D Varying Grid Diffusion'
    if variant == 'R':
        title += ', R diffusion'
    plt.title(title)
    plt.legend()
    if method in ['steps', 't']:
        plt.xscale('log')
    plt.yscale('log')
    plt.savefig('OneDimVarDiff%sError_%s.pdf' % (variant, method))
    return 0


def OneDimVarGrid(file_appendix='toy_x'):
    file_list = glob.glob('OneDimVarGrid_%s*.csv' % file_appendix)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Grid ' + file_appendix
    plt.title(title)
    plt.legend()
    if file_appendix in ['t', 'steps']:
        plt.xscale('log')
    plt.yscale('log')
    plt.savefig('OneDimVarGrid_%s.pdf' % file_appendix)
    return 0


def Advection():
    file_list = glob.glob('Advection_*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Advection Test of numeric algorithms'
    plt.title(title)
    plt.legend()
    plt.savefig('Advection.pdf')
    return 0


def main():
    print('Start creating plots.')
    t_start = time.time()

    for i in [1, 2]:
        OneDimVarDiff(i)
        OneDimVarDiffR(i)
    OneDimVarError('steps')
    OneDimVarError('t')
    OneDimVarError('steps', 'R')
    OneDimVarError('t', 'R')

    for s in ['z', 'r', 'logp', 't', 'steps']:
        OneDimVarGrid(s)

    Advection()

    t_end = time.time()
    print('Process finished in %f seconds.' % (t_end - t_start))
    plt.show()
    return 0


if __name__ == '__main__':
    main()
