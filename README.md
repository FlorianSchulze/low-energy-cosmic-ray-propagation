# Low Energy Cosmic Ray Propagation

!!!THE README IS OUT OF DATE!!!

The program code for numerical simulation.

For new changes also thee see CHANGELOG.md!

By @FlorianSchulze

## Programming language
The program is writen in c++14 standard.
It is compiled with cmake on the Linux Subsystem for Windows (WSL).
The plots are made with python and the matplotlib via the script `plot.py` or `plots.py`.
The templates are saved in the `.hdf5` format.

## Objective
The program should solve the

* one dimensional diffusion equation (OneDimDiffusion)
* three dimensional diffusion equation with rotational symmetry (TwoDimDiffusion)
* three dimensional diffusion equation with rotational symmetry and homogeneous energy loss (ThreeDimDiffusion)

numerical and compare the results with the analytical green's function.

It also should calculate the green's function of the diffusion of cosmic rays (CR)
and save the data in `.hdf5` templates.

## Theory
### One dimensional diffusion (OneDimDiff)
#### x coordinate
The program should numerical solve the diffusion equation in 1D:

    du/dt = kappa0 (d^2u/dx^2)
    u(-L) = u(L) = 0

The greens function for the equation is:

    u(x,t) = exp(-x^2 / (4 t kappa0)) / (2 sqrt(pi t kappa0))
    u(t=0) = dirac_delta(x)
    
#### r coordinate
The program should numerical solve the diffusion equation in 1D in an r coordinate:

    du/dt = kappa0 (1/x du/dx + d^2u/dx^2)
    u(L) = 0

The greens function for the equation is:

    u(x,t) = exp(-x^2 / (4 t kappa0)) / (4 pi t kappa0)
    u(t=0) = dirac_delta(x)

### One dimensional flux conservative equation (NoneDiffusive)
The program should numerical solve the 1D flux conservative equation (non diffusive equation):

    du/dt = -v * du/dx
    u(x=-L) = u(x=L) = 0

As a solution and test function for the equation is used:

    u(x,t) = dirac_delta(x - v t)
    dirac_delta(x) = exp(-x^2 / (2 epsilon)) / sqrt(2 pi epsilon) 
 
### Three dimensional diffusion with rotational symmetry (TwoDimDiff)
The program should numerical solve the 3D diffusion equation
with rotational symmetry, so that only the variables `r` and `z` appear:

    du/dt = kappa0 (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2)
    u(r=R) = 0
    u(z=-L) = u(z=L) = 0

The greens function for the equation is:

    u(r,z,t) = exp(-(r^2 + z^2) / (4 t kappa0)) / (4 pi t kappa0)^(3/2)
    u(t=0) = 1/(2 pi) dirac_delta(r) dirac_delta(z)

### Three dimensional diffusion with homogeneous energy loss (ThreeDimDiff)
The program should numerical solve the 3D diffusion equation
with rotational symmetry and homogeneous energy loss,
so that only the variables `r`, `z` and `p` appear:
    
    du/dt - kappa(p) Laplace(u) + d/dp (b(p)*u) = 0
    u(p=p0) = dirac_delta(x - x0) dirac_delta(t-t0)
    kappa = kappa0 * (p/p0)^delta
    b = -b0 * (p/p0)^2 
    
The variables are set dimensionless, `t0` and `r0` are set `0`
and `log_p = log(p)` is chosen as variable instead of `p`, 
so the equation is:

    du/dt = exp(delta log_p) (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2)
          + exp(-log_p) d/dlog_p (exp(2 log_p) u)

The greens function for the equation is:

    u(r,z,log_p,t) = exp(-(r^2+z^2)/(4 lambda)) / (4 pi lambda)^(3/2)
                     * 1/p^2 dirac_delta(p - 1/(t + 1))
    lambda = 1/(1-delta) * ((t+1)^(1-delta) - 1);

### Cosmic ray diffusion (CRDiff)
The program should numerical solve the comic ray (CR) diffusion equation.
The variables are used as dimensionless variables:
    
    du/dt - kappa Laplace(u) + d/dp (b*u) Heaviside(z<=h) = 0
    u(r=R) = 0, u(z=-L) = u(r=L) = 0
    u(t=0) = DiracDelta(r-r0) DiracDelta(t-t0) q(p)
    kappa = p^delta, b = -p^n, q = p^-gamma

## Problems with the analytic results
Because this fit the boundary conditions only for `L -> inf`, `R -> inf`.
So `L`, `R` must be big for the numeric results to fit the analytic ones.
This is an source of errors between the analytic and numeric calculation. 

An other problem is the not finite initial condition `u(t=0) = dirac_delta(x)`.
To calculate an the dirac delta numerical, the green's function taken at `t = offset * dt`.
So it is approximated by the normal distribution with the small parameter `offset * dt`.
In this way the analytical and numerical solution can be compared,
by comparing the numerical solution after `steps` time steps `dt`
with the analytical function at `t = dt * (steps + offset)`. 

## Chosen parameters
The parameters are saved in as macros in `paramters.hpp`.
There are two sets of parameters. 
One set are toy parameters for testing the numeric algorithms.
Plots with this parameters are labeled with `_1`
The other set is with parameters like in the `lightcurve` plot.
The plots with this parameters are labeled with `_2`.

## Explanation of the plots
The plots are in the folder `plots`.

Plots with `_1` use the toy parameters, `_2` use the second set of parameters. 

`*_error.pdf` shows the relative error for the plot `*.pdf`.

`*Error_dt.pdf` / `*Error_steps.pdf` show the relative value of the maximal error
for the `*` equation as a function of the parameter `dt` / `steps`.

* `OneDimDiff`: one dimensional diffusion in an regular x coordinate
* `OneDimDiffR`: one dimensional diffusion in an r coordinate
* `NonDiffusive`: results of different methods for the flux conservative equation
at different time steps
* `TwoDimDiff`: three dimensional diffusion with rotational symmetry  
`*_r`: r coordinate at z=0  
`*_z`: z coordinate at r = 0
* `ThreeDimDiff`: three dimensional diffusion with energy loss  
`*_r`, `*_z`: like before, at log_p = ??  
`*_logp`: log_p coordinate at r=0, z=0
* `ThreeDimTestDiff`: Test of the diffusion part in `ThreeDimDiff.cpp`
using the theory for TwoDimDiff
* `ThreeDimTestP`: Test of the energy loss part in `ThreeDimDiff.cpp`
using the solution for the diffusion `u = exp(-2 x) * dirac_delta(exp(-x) - (t + 1))`
* `CRDiffTest`: Tests the CR diffusion, reproducing the results of ThreeDimDiff
* `CRDiff`: Plot the results of the calculation of CRDiffusion as a function of `r, z, log_p`
* `CRDiffPlot`: Try to recreate the `lightcurve` plot,
but have a very small time and energy interval, to have a fast simulation.
I don't exactly know how big the errors are.

## Explanation of the program code
### NDVector.cpp / .hpp
An class that represent an N dimensional vector of an arbitrary type `T`.
It can be created in the following ways:

* `NDVector<T> vec(N, {n1, n2, .., nN})` creates an `N` dimensional
vector of the size `{n1, n2, ..., nN}` in x1, x2, ..., xN direction.
* `NDVector<T> vec({x1, x2, ..})`, `NDVector<T> vec = {x1, x2, ..}`
creates a one dimensional vector with the values `{x1, x2, ...}`.
* `NDVector<T> vec(stdvec)`, `NDVector<T> vec = stdvec`
with `stdvec` element of `std::vector<T>`
creates a one dimensional vector with the values `stdvec`.

The data of the n dimensional vector is saved in an `std::vector<T>`
with the size `n1 * n2 * ... * nN`.
The data be accessed in the following ways:

* `vec[i]` access the `i` element in the underlying vector.
* `vec[{i1, i2, ..., iN}]` access the element `{i1, i2, ..., iN}`
of the `N` dimensional vector.
* `vec.at(i)` access the `i` element in the underlying vector with boundary checks.
Negative index stand for backward counted index.
* `vec.at({i1, i2, ..., iN})` access the element `{i1, i2, ..., iN}`
of the `N` dimensional vector with boundary checks.
Negative index stand for backward counted index.
* `vec.at((std::size_t) index_in_subvector, subvector_index, index)`
gets the element at `{index0, index1, ...}`,
but with `index[subvectorindex] = index_in_subvector` without boundary check.
* `vec.at((int) index_in_subvector, subvector_index, index)`
gets the element at `{index0, index1, ...}`,
but with `index[subvectorindex] = index_in_subvector` with boundary check.
Negative index stand for backward counted index.

The `NDVector.cpp` file is included in `NDVector.hpp` and shouldn't
be precompiled and linked, because of the use of `template`.

### plot.cpp / .hpp
Here some general functions are defined,
specially for calculation with vectors.
For example `arrange(start, stop, step)` which creates a vector
(see [numpy.arrange](https://docs.scipy.org/doc/numpy/reference/generated/numpy.arange.html)).

The types `NumType = double`, `VecNum = std::vector<NumType>` and
`VecND = NDVector<NumType>` are defined here,
so they can be used in the complete program and easily changed.
`NumType_I = const double`, `VecNum_I = const std::vector<NumType>` and
`VecND_I = const NDVector<NumType>` are defined for input parameter to functions.

`save_as_csv(x, y, file, x_label, y_label, label)` is a function to save
vectors as a `.csv` file, so they can be plotted by `plot.py`
(see description below).

`save_as_csv(x, file)` is a function to save
a `NDvector` as a `.csv` file, so they can be load by `plot.py`
(see description below).

### plot.py
Creates an plot from one or more `.csv` files.
The `.csv` files must have the form described below.
For more information and help type `python plot.py ?`.

### plots.py
Creates all plots that should be shown.
Uses `plot.py` but have some more freedom in generating the plots. 

### CRDiffusion.py
Handle the `greens_template` of CRDiffusion.

### own_error.cpp / .hpp
Implements an exception `own_error` that can be given an `const char*` string as message.
`THROW(message)` throws such an exception.

### tridiag.cpp / .hpp
Solves a tridiagonal matrix system with the Thomas algorithm.

### NDNumerics.cpp / .hpp
Implement different numerical methods to solve differential equations,
using the `NDVector` class and it's method `apply_subvector`.
The methods solve one dimensional problems,
but for every subvector in an `NDVector`.
The dimension in that the equation should be solved is given
via the parameter `dimension_index`.

### OneDimDiffusion.cpp / .hpp
Uses the Crank-Nicolson methods implemented in NDNumerics to solve the diffusion equation.

The diffusion equation in an r coordinate is solved with operator splitting:

    du/dt = kappa d^2u/dx^2 (Crank-Nicloson)
          + kappa/x du/dx   (Semi-Lagrangian scheme with v = -kappa/x)

Also implements the functions for the non-diffusive problem.

### TwoDimDiffusion.cpp / .hpp
Solves the three dimensional diffusion problem with rotational symmetry,
that has only the two independent variables `r` and `z`.
With the use of operator splitting the equation is break down
to three time steps.
These are solved with the methods provided in `NDNumerics`:

    1. du/dt = kappa0 d^2u/dz^2 (Crank-Nicolson in z with constant boundaries at -L and L)
    2. du/dt = kappa0 d^2u/dr^2 (Crank-Nicolson in r with constant boundary at R, for r=0 r[-j] = r[j] is used)
    3. du/dt = kappa0 1/r du/dr (Semi-Lagrangian in r with constant boundaries at 0 and R)

### ThreeDimDiffusion.cpp / .hpp
Solves the three dimensional diffusion problem with energy loss,
uses the three independent variables `r`, `z` and `log_p = ln(p)`.
The diffusive part is solved as before,
but with a not constant the diffusion coefficient.
The energy loss part is solved by writing:

    du/dt = exp(-log_p) d/dlog_p (exp(2 log_p) u)
          = 2 exp(log_p) u + exp(log_p) du/dlog_p

This is solved via operator splitting,
by using an semi-lagrangian upwind scheme for the differential in `log_p`
and then update `u` with the other term.

### CRDiffusion.cpp / .hpp
Solves the CR diffusion equation numerical.
Uses the same methods as before, only the injection term is a different one.

### main.cpp
Brings the upper parts together and creates the plots.

### .csv files
The `.csv` files created from `plot.cpp` and
needed for `plot.py` have the following form:

1. line: label of the graph
2. line: x_label; y_label
3. line - end: x; y

### .hdf5 files
There are two types of `.hdf5` files.  
`CRDiff*` saves the NDVector `u` (and `u_theory` for `CRDiffTest`) as a function of `log_p, z, r`.  
`greens_template*` saves the NDVector u as a function of `t, log_p, r at z=0`.  
The variables `log_p, r, t / z` are also saved in the file.

### Makefile
Compiles the code. There are two special commands in the Makefile:
* `make clean_all` delete all files created by cmake
* `make clean_plot` delete all `*.csv, *.pdf, *.hdf5` files, but not the ones in `plot`
