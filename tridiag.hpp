//
// Created by Flo on 01.05.2020.
//

#ifndef LECRP_TRIDIAG_HPP
#define LECRP_TRIDIAG_HPP
#include "plot.hpp"
#include "own_error.hpp"

/**
 * Solves a tridiagonal matrix system with the Thomas algorithm.
 * @param[in] a, b, c define the tridiagonal matrix {{b0, c0, ...}, {a1, b1, c1, ...}, ..., {..., an, bn}}
 * @param[in] r the vector on the righthand site (m * u = r)
 * @param[out] u the vector to solve for
 * @throws own_error thrown if the algorithm doesn't work because of zeros
 * @see Numerical Recipes p. 56
 */
void tridag(VecNum_I &a, VecNum_I &b, VecNum_I &c, VecNum_I &r, VecNum &u);


/**
 * Solves a "cyclic" tridiagonal matrix system.
 * @param[in] a, b, c define the tridiagonal matrix {{b0, c0, ...}, {a1, b1, c1, ...}, ..., {..., an, bn}}
 * @param[in] alpha, beta corner entries in the matrix. alpha: left/down, beta: up/right
 * @param[in] r the vector on the righthand site (m * x = r)
 * @param[out] x the vector to solve for
 * @throws own_error thrown if the algorithm doesn't work because of zeros
 * @relatesalso tridag
 * @see Numerical Recipes p. 79
 */
void cyclic(VecNum_I &a, VecNum_I &b, VecNum_I &c, NumType_I &alpha, NumType_I &beta, VecNum_I &r, VecNum &x);

#endif //LECRP_TRIDIAG_HPP
