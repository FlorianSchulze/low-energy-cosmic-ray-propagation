"""
Create the final plots for the project.
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, integrate
import glob
import h5py
import time
import multiprocessing
from Templates import *
from Stochastic import *


def plot_energy_loss():
    """Plot the energy loss b(p)"""
    file_list = glob.glob('CRDiff_energy_loss*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        log_p, dpdt = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        dpdt = dpdt * np.exp(log_p) * b0
        dEdt = dpdt / np.sqrt(1.0 + (mass_p / p) ** 2)
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        # print('Ekin: ', ekin)
        index_ekin = np.argmin(np.abs(ekin - 1.0))
        # print('-dE/dt(Ekin=%fMeV) = %f MeV/Myr' % (ekin[index_ekin], -dEdt[index_ekin]))
        label = 'composed'
        # plt.plot(ekin, -dEdt, label=label)
        plt.plot(p, -dpdt, label=label)

        """Fit power law"""

        def energy_loss_analytic(par_p, par_n):
            return b0 * np.power(par_p / p0, par_n)

        n_fit = n_analytic
        # from scipy import optimize
        # out = optimize.curve_fit(energy_loss_analytic, p, dpdt, [n_fit])
        # print(out)
        # n_analytic = out[0][0]
        energy_loss_analytic_val = energy_loss_analytic(p, n_fit)
        # energy_loss_analytic_val = energy_loss_analytic_val / np.sqrt(1.0 + (mass_p / p) ** 2)

        label = r'$b(p) = b_0 (\frac{p}{p0})^n, n = %.2f$' % n_analytic
        # plt.plot(ekin, energy_loss_analytic_val, label=label)
        plt.plot(p, energy_loss_analytic_val, label=label)
    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    # plt.ylabel('$-\\frac{dE}{dt}$ [$\\frac{MeV}{Myr}$]')
    plt.ylabel('$-\\frac{dp}{dt}$ [$\\frac{MeV/c}{Myr}$]')
    # plt.title('Energy loss rate')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.savefig('CRDiffEnergyLoss.pdf')
    return 0


def plot_diffusion_coefficient():
    """Plot the diffusion coefficient kappa(p)"""
    file_list = glob.glob('CRDiff_diffusion_coefficient*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        log_p, kappa = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        kappa = kappa * kappa0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        index_ekin = np.argmin(np.abs(ekin - 1.0))
        # print('kappa(Ekin=%fMeV) = %f kpc^2/Myr' % (ekin[index_ekin], kappa[index_ekin]))
        label = 'composed'
        # plt.plot(ekin, kappa, label=label)
        plt.plot(p, kappa, label=label)

        """Fit power law"""

        def kappa_analytic(par_p, par_delta):
            return kappa0 * np.power(par_p / p0, par_delta)

        delta_fit = delta_analytic
        # from scipy import optimize
        # out = optimize.curve_fit(kappa_analytic, p, kappa, [delta_fit])
        # print(out)
        # delta_fit = out[0][0]
        kappa_analytic_val = kappa_analytic(p, delta_fit)
        label = r'$\kappa(p) = \kappa_0 (\frac{p}{p_0})^{\delta}, \delta = %.2f$' % delta_fit
        # plt.plot(ekin, kappa_analytic_val, label=label)
        plt.plot(p, kappa_analytic_val, label=label)
    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    plt.ylabel('$\\kappa$ [$\\frac{kpc^2}{Myr}$]')
    # plt.title('Diffusion coefficient')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.savefig('CRDiffDiffusionCoefficient.pdf')
    return 0


def plot_characteristic_time_scales():
    """Plot the characteristic time scales"""
    file_list_energy_loss = glob.glob('CRDiff_energy_loss*.csv')
    file_list_diffusion = glob.glob('CRDiff_diffusion_coefficient*.csv')
    if not file_list_energy_loss and not file_list_diffusion:
        return -1
    plt.figure()
    for file in file_list_energy_loss:
        log_p, dpdt = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        dpdt = dpdt * np.exp(log_p) * b0
        dpdt_analytic = b0 * np.power(p / p0, n_analytic)
        t = p / abs(dpdt)
        t_analytic = p / abs(dpdt_analytic)
        # obj = plt.plot(ekin, t, label='Energy loss time')
        obj = plt.plot(p, t, label='energy loss time')
        # plt.plot(ekin, t_analytic, '--', color=obj[-1].get_color())
        plt.plot(p, t_analytic, '--', color=obj[-1].get_color())
    for file in file_list_diffusion:
        log_p, kappa = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        kappa = kappa * kappa0
        kappa_analytic = kappa0 * np.power(p / p0, delta_analytic)
        """loss out of disk"""
        t = h ** 2 / kappa
        t_analytic = h ** 2 / kappa_analytic
        # obj = plt.plot(ekin, t, label='Diffusion out of disk')
        obj = plt.plot(p, t, label='diffusion out of disk')
        # plt.plot(ekin, t_analytic, '--', color=obj[-1].get_color())
        plt.plot(p, t_analytic, '--', color=obj[-1].get_color())
        """loss out of halo"""
        t = L ** 2 / kappa
        t_analytic = L ** 2 / kappa_analytic
        # obj = plt.plot(ekin, t, label='Diffusion out of halo')
        obj = plt.plot(p, t, label='diffusion out of halo')
        # plt.plot(ekin, t_analytic, '--', color=obj[-1].get_color())
        plt.plot(p, t_analytic, '--', color=obj[-1].get_color())
    plt.plot(0, 0, '--', color='grey', label='power law')
    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    plt.ylabel(r'$\tau_{loss}$ [Myr]')
    # plt.title('Characteristic time scales')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.savefig('CRDiffusionCharacteristicTimes.pdf')
    return 0


def plot_error_one_dimensional_diffusion(variable='z'):
    """Plot the error of the one dimensional Diffusion equation in 'variable' ('z' or 'r')."""
    if variable == 'z':
        file_list = glob.glob('OneDimDiffError_steps_*.csv')
    else:
        file_list = glob.glob('OneDimDiffRError_steps_*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        steps, du = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        dx = file.split('_')[2][2:]
        dt = file.split('_')[3][2:-4]
        dx = float(dx) * lamb
        dt = float(dt) * tau
        label = r'$\Delta %s$=%.3fkpc, $\Delta t$=%.eMyr' % (variable, dx, dt)
        plt.plot(steps, du, label=label)
    # plt.title('Error of Diffusion - $%s$' % variable)
    plt.xlabel('steps $n$')
    plt.ylabel(r'$\Delta u$')
    loc = 'upper right'
    if variable == 'z':
        loc = 'center left'
    plt.legend(loc=loc)
    plt.xscale('log')
    plt.yscale('log')
    # plt.grid()
    plt.tight_layout()
    plt.savefig('ErrorDiffusion_%s.pdf' % variable)
    return 0


def plot_error_one_dimensional_diffusion_varying_grid(method='steps'):
    """Plot the error of the one dimensional Diffusion equation for a varying gird in 'variable' ('z' or 'r')."""
    file_list_r = glob.glob('OneDimVarDiffRError_%s_*.csv' % method)
    file_list_z = glob.glob('OneDimVarDiffError_%s_*.csv' % method)
    if not file_list_r and not file_list_z:
        return -1
    plt.figure()
    for file in file_list_r:
        x, du = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        if method == 't':
            x = x * tau
        label = 'r'
        plt.plot(x, du, label=label)
    for file in file_list_z:
        x, du = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        if method == 't':
            x = x * tau
        label = 'z'
        plt.plot(x, du, label=label)
    if method == 'steps':
        plt.xlabel('steps $n$')
    elif method == 't':
        plt.xlabel('t [Myr]')
    plt.ylabel(r'$\Delta u$')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    # plt.grid()
    plt.tight_layout()
    plt.savefig('ErrorVarDiffusion_%s.pdf' % method)
    return 0


def main():
    plt.rcParams.update({'font.size': 10})
    print('Start CRDiffusion.py with %i cores.' % multiprocessing.cpu_count())
    t_start = time.perf_counter()

    # convert_dat_to_hdf5('fEr_p.dat', '_Minh')
    # convert_dat_to_hdf5('fEr_p.dat', '_Minh', dlogr=0.05, dlogt=0.02, dlogt_scale=0.1)

    # plot_template('_Test_zmax',pos_t=[1e-1, 1e0, 1e2], pos_p=[1e0, 1e2, 1e4], pos_r=[0], ylim=None, use_ekin=True, do_plot_analytic=True)
    # plot_template('_Flo', pos_t=[1e-1, 1e0, 1e2], pos_p=[1e0, 1e2, 1e4], pos_r=[0], ylim=None, use_ekin=True)
    # plot_template('_Minh', pos_t=[0], pos_p=[0], pos_r=[0], ylim=None, use_ekin=True)
    # plot_template('_Flo', pos_t=[0], pos_p=[1e1], pos_r=[0], ylim=None, use_ekin=True)
    # plot_template('_TEST_2', pos_t=[1e-2], pos_p=[], pos_r=[0], ylim=None, use_ekin=True, do_plot_analytic=True)

    # compare_greens_template('_TEST_Ini', '_Minh', pos_t=[0], pos_p=[1e1], pos_r=[0])
    # compare_greens_template('_TEST_Ini', '_Flo', pos_t=[0], pos_p=[1e1], pos_r=[0])
    # compare_greens_template(['_TEST_Ini', '_Flo'], pos_t=[0], pos_p=[1e1, 2e2], pos_r=[0], use_ekin=True)
    # compare_greens_template('Minh', 'Test_7', pos_t=[1e1], pos_p=[1e2, 1e3, 1e4], pos_r=[0], ylim=[1e44, 1e50])
    # compare_greens_template('Minh', 'Flo', pos_t=[1e1], pos_p=[1e2, 1e3, 1e4], pos_r=[0], ylim=[1e44, 1e50])

    t_end = time.perf_counter()
    print('Process finished in %f seconds.' % (t_end - t_start))
    return 0


if __name__ == '__main__':
    main()
