#!/bin/bash
CPPC=h5c++
CPPC_FLAGS="-Wall -Wextra -pedantic -O3 -std=c++11 -fopenmp"
CPPC_LINKER_FLAGS=
BUILD_DIR="build/"
CPP_FILES="own_error.cpp plot.cpp tridiag.cpp NDNumerics.cpp parameters.cpp OneDimDiffusion.cpp CRDiffusion.cpp"
PY_FILES="plot.py plots.py Templates.py Stochastic.py CRDiffusion.py"

OUT_FILES=
for file in ${CPP_FILES}
do
  ${CPPC} ${CPPC_FLAGS} -o ${BUILD_DIR}/${file}.o -c ${file}
  OUT_FILES="${OUT_FILES} ${BUILD_DIR}/${file}.o"
done

${CPPC} ${CPPC_FLAGS} -o ${BUILD_DIR}/main.exe main.cpp -L. ${OUT_FILES} ${CPPC_LINKER_FLAGS}

cp parameters.dat ${BUILD_DIR}/parameters.dat

for file in ${PY_FILES}
do
  cp ${file} ${BUILD_DIR}/${file}
done