# Changelog

All notable changes to this project will be documented in this file.

## 29.11.2020
Included paramters.dat like in Minhs project. Not used until now!

Included python files in cmake build. Changed compiler settings.

## 27.11.2020
Removed files of Minhs_project. They are in a own project: https://git.rwth-aachen.de/vohongminh.phan/cres

## 20.11.2020
Changed the scheme for calculation in of the energy loss part to the new own advection_scheme.
Changed the t grid to a logarithmic grid. Chose a smaller step size for the log_p grid.
Made the grid in r and z much coarser.

## 19.11.2020
Implemented many new advection schemes in NDNumerics:
* mpdata_2 - the mpdata scheme but with a second correction step
* Crank-Nicolson - a simple Crank-Nicolson scheme for constant velocity
* advection_scheme - an own aovectoin scheme, that is an Crank-Nicolson upsind scheme.

Implemented new funciton for matirx multiplication in NDNumerics:
'matrix_multiplication_explicit' and 'matrix_multiplication_implicit'

Added some functionalities to 'plot_template' in 'Templates.py'.

## 17.11.2020
Finished implementation analytic solution.

## 16.11.2020
Added an analytic solution with energy loss everywhere in Templates.py. Still has some problems.

Split the 'CRDiffusion.py' in two the different python files 'Templates.py', 'Stochastic.py' and 'CRDiffusion.py'

Tested the logarithmic grid. It works fine!

## 14.11.2020
Changed z, r grid to logarithmic grid.

Moved old code files to the folder "old". Removed not needed code fragments in the current code.
TODO: Update Readme!

Removed constant diffusion at one energy.

## 31.10.2020
Added function 'plot_integrated_p' in CRDiffusion.py, to analyze the constant diffusion test.

## 28.10.2020
Use only a constant diffusion coefficient and injection at only one energy.
Hardwired in the code, comment it out afterwards! 

## 22.10.2020
Many changes. Mainly to compare better with Minh.

## 21.10.2020
Use Minhs initial condition in the calculation of the Green's function.

## 20.10.2020
Fixed an error on Minh code that lead to bad_alloc errors.

## 19.10.2020
Included Minhs program code (Minh_template.cpp, Minh_generate_sample.cpp).

Implemented python function to converts Minhs .dat file into .hdf5 files. (convert_dat_to_hdf5 in CRDiffusion.py)

Changed the parameters of source distribution to Minhs.

## 18.10.2020
Don't use parallel python code because problems on the Cluster.

The calculation of the different spectrum samples now runs in parallel.
Changed the random generator, so (hopefully) there are no problems with the parallelization.

## 17.10.2020
Added function spectrum_mean. Calculates the mean via riemann integration of the Green's template.

## 16.10.2020
Added function generate_sample in CRDiffusion.py

Some smaller changes. Adjusted the CRDiffusion.py for the varying grid.

## 15.10.2020
Corrected a critical error in the implicit diffusion scheme with varying grids.

## 12.10.2020
The z grid is now only positive, because it is symmetric in 0.

## 11.10.2020
Implemented non-equidistant grids for CRDiffusion.
Added function create_grid in main.cpp.

## 10.10.2020
Added implementation of non-equidistant t steps for the 1D Diffusion.
Added function onedim::varying_grid_error.

## 09.10.2020
Added a diffusion Crank-Nicolson scheme and a Semi-Lagrangian scheme for non-equidistant grids.

Added methods for testing the non-equidistant gird in one dimensional diffusion problems
(varying_grid_plot, varying_grid_plot_r in OneDimDiffusion).

Added some methods in plot.cpp for simpler creation of non-equidistant grids.

Some minor changes in the CMake file.

## 19.09.2020
Copied the Project from the Bachelor Thesis: https://git.rwth-aachen.de/FlorianSchulze/bachelorarbeit