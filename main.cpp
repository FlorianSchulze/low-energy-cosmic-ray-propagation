//
// Created by Flo on 01.05.2020.
//

#include "parameters.hpp"
#include "own_error.hpp"
#include "NDVector.hpp"
#include "plot.hpp"
#include "OneDimDiffusion.hpp"
#include "CRDiffusion.hpp"
#include <iostream>
#include <cmath>
#include <ctime>
#include <hdf5.h>
#include <omp.h>

using namespace std;
using namespace plot;

/**
 * Creates the Grids in r, z, log_p, t and steps for the calculation.
 * @param[out] r, z, log_p, t, steps Vector
 */
void create_grid(VecNum &r, VecNum &z, VecNum &log_p, VecNum &t, VecNum &steps) {
    parameter::load_parameters();

//    z = vec_apply(linspace(log(DZ), log(L), 200), exp);
    z = vec_apply(linspace(log(DZ), log(L), 100), exp);
    z.insert(z.begin(), 0);
//    VecNum dz = create_vector({DZ, DZ * 2, DZ * 2, DZ * 10, DZ * 10}, {401, 0, 301, 0, 301});
//    z = create_vector_from_dx(dz, 0, 0); ///only positive grid
//    z = arange(0, L, DZ);
//    test_stable_dx(calculate_dx(z));
//    save_as_csv(z, calculate_dx(z), "OneDimVarGrid_z.csv", "z", "$\\Delta z$", "Grid z");
//    cout << "L = " << L * LAMBDA << "kpc = " << z.back() * LAMBDA << "kpc" << endl;
//    cout << "DZ = " << DZ * LAMBDA << "kpc = " << (z[1] - z[0]) * LAMBDA << "kpc" << endl;
//    cout << "z.size() = " << z.size() << endl;

//    r = vec_apply(linspace(log(DR), log(R), 1000), exp);
    r = vec_apply(linspace(log(DR), log(R), 200), exp);
    r.insert(r.begin(), 0);
//    VecNum dr = create_vector({DR, DR * 2, DR * 2, DR * 10, DR * 10, DR * 50, DR * 50}, {401, 0, 301, 0, 301, 0, 321});
//    r = create_vector_from_dx(dr, 0, 0);
//    r = arange(0, R, DR);
//    test_stable_dx(calculate_dx(r));
//    save_as_csv(r, calculate_dx(r), "OneDimVarGrid_r.csv", "r", "$\\Delta r$", "Grid r");
//    cout << "R = " << R * LAMBDA << "kpc = " << r.back() * LAMBDA << "kpc" << endl;
//    cout << "DR = " << DR * LAMBDA << "kpc = " << (r[1] - r[0]) * LAMBDA << "kpc" << endl;
//    cout << "r.size() = " << r.size() << endl;

    log_p = arange(log(P_START), log(P_END), DLOG_P);
//    save_as_csv(log_p, calculate_dx(log_p), "OneDimVarGrid_logp.csv", "log_p", "$\\Delta$log_p", "Grid log_p");
//    cout << "p_start = " << P_START * P0 << "MeV = " << exp(log_p.front()) * P0 << "MeV" << endl;
//    cout << "p_end = " << P_END * P0 << "MeV = " << exp(log_p.back()) * P0 << "MeV" << endl;
//    cout << "DLOG_P = " << DLOG_P << " = " << (log_p[1] - log_p[0]) << endl;
//    cout << "log_p.size() = " << log_p.size() << endl;

    t = vec_apply(linspace(log(T_START), log(T_END), 1000), exp); ///Does not work for times > 1Myr
//    VecNum dt = create_vector({DT_MIN, DT_MIN * 10, DT_MIN * 10, DT_MAX, DT_MAX}, {91, 0, 91, 0, 19981});
//    t = create_vector_from_dx(dt, T_START);
//    save_as_csv(t, calculate_dx(t), "OneDimVarGrid_t.csv", "t", "$\\Delta t$", "Grid t");
//    cout << "t_start = " << T_START * TAU << "Myr = " << t.front() * TAU << "Myr" << endl;
//    cout << "t_end = " << T_END * TAU << "Myr = " << t.back() * TAU << "Myr" << endl;
//    cout << "DT_MIN = " << DT_MIN * TAU << "Myr = " << (t[1] - t[0]) * TAU << "Myr" << endl;
//    cout << "DT_MAX = " << DT_MAX * TAU << "Myr = " << (t.back() - t[t.size() - 2]) * TAU << "Myr" << endl;
//    cout << "t.size() = " << t.size() << endl;

//    VecNum t_log = vec_apply(arange(log10(T_START), log10(T_END), DLOG_T), exp10);
    VecNum t_log = vec_apply(arange(log10(T_START), log10(1e-2 / TAU), DLOG_T), exp10);

    steps = VecNum(t_log.size() + 1);
    VecNum t_log2 = VecNum(t_log.size() + 1);
    steps[0] = 0;
    t_log2[0] = t[0];
    NumType step = 1;
    for (SizeType i = 0; i < t_log.size(); i++) {
        while (step < t.size() - 1 and t[step] < t_log[i]) {
            step++;
        }
        steps[i + 1] = step;
        t_log2[i + 1] = t[(int) step];
    }
//    save_as_csv(t_log2, calculate_dx(t_log2), "OneDimVarGrid_t_log2.csv", "t", "$\\Delta t$", "Grid t_log2");
//    save_as_csv(steps, calculate_dx(steps), "OneDimVarGrid_steps.csv", "steps", "$\\Delta steps$", "Steps");

//    steps = {0, 1, 1, 2, 3, 4, 5, 6, 8, 10}; ///Test steps
}

int main() {
//    omp_set_num_threads(7); ///Limit CPU usage, so I can work while the program is running
    double t_start, t_end;
    t_start = omp_get_wtime();

    try {
        printf("Start program with %d maximal threads.\n", omp_get_max_threads());

        ///Create Grid
        VecNum r, z, log_p, t, steps;
        create_grid(r, z, log_p, t, steps);

        ///OneDim Varying Grid real parameter
//        onedim::varying_grid_plot(z, t, 1, "_2", 5e0 / TAU);
//        onedim::varying_grid_plot_r(r, t, 1, "_2", 5e0 / TAU);
//        onedim::varying_grid_error(steps, z, t, 1, "_2");
//        onedim::varying_grid_error_r(steps, r, t, 1, "_2");

        ///OneDim advection
//        onedim::advection_plot(1e-1, 1e-1, 100, -15, 3, -1, 1e-1, "");

        ///Generate Templates for the CRDiffusion
//        CRDiff::generate_template_varying_grid(r, z, log_p, t, steps, H, DELTA, POWER_N, GAMMA, "_TEST_2");
//        CRDiff::generate_template_varying_grid(r, z, log_p, t, steps, L, DELTA, POWER_N, GAMMA, "_TEST_2");
    } catch (const own_error &err) {
        own_catch(err);
    }

    t_end = omp_get_wtime();
    double duration_sec = t_end - t_start;
    cout << "Process finished in " << duration_sec << " seconds." << endl;

    return 0;
}
