//
// Created by Flo on 04.05.2020.
//

#ifndef LECRP_ONEDIMDIFFUSION_HPP
#define LECRP_ONEDIMDIFFUSION_HPP

#include "plot.hpp"

/**
 * Some algorithms to solve the one dimensional diffusion equation: <BR>
 * in an x coordinate: du/dt = kappa0 * d^2u/dx^2 <BR>
 * in an r coordinate: du/dt = kappa0 * (1/r du/dx + d^2u/dx^2) <BR>
 * and the one dimensional non-diffusive problem: <BR>
 * du/dt = -v * du/dx <BR>
 * The initial values and boundary conditions must be set in u.
 * @relatesalso NDVector, NDNumerics
 */
namespace onedim {
    /**
     * Theoretical value of the flux, with dirac_delta(x) at t = 0.
     * @param x, t
     * @param kappa0 diffusion coefficient
     * @return exp(-x^2 / (4 t kappa0)) / (2 sqrt(pi t kappa0))
     */
    NumType psi_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0);

    /**
     * Theoretical value of the flux in r, with dirac_delta(x) at t = 0.
     * @param x, t
     * @param kappa0 diffusion coefficient
     * @return exp(-x^2 / (4 t kappa0)) / (4 pi t kappa0)
     */
    NumType psi_r_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0);


    /**
     * Generates .csv files for the advection problem. <BR>
     * du/dt = -v * du/dx <BR>
     * u(t=0) = dirac_delta(x, epsilon)
     * @param dx, dt step size
     * @param steps count of steps to calculate
     * @param x_start, x_end x start/end
     * @param v velocity
     * @param epsilon epsilon in dirac_delta
     * @param file_appendix the appendix to the file to identify different plots
     */
    void advection_plot(NumType_I &dx, NumType_I &dt, const int &steps, NumType_I &x_start, NumType_I &x_end,
                        NumType_I &v, NumType_I &epsilon, const std::string &file_appendix = "");

    /**
     * Calculate one-dimensional diffusion in a normal coordinate with non-equidistant grid and saves the plots as .csv. <BR>
     * du/dt = kappa0 * d^2u/dx^2
     * @param x x grid - only positive values!
     * @param t t grid. t[0] is offset. step size is t[i] - t[i-1].
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix to the file to identify different plots
     * @param t_end end time of calculation, -1 for complete t grid
     */
    void varying_grid_plot(VecNum_I &x, VecNum_I &t, NumType_I &kappa0, const std::string &file_appendix,
                           NumType t_end = -1);

    /**
     * Calculate one-dimensional diffusion in the r coordinate with non-equidistant grid and saves the plots as .csv. <BR>
     * du/dt = kappa0 * (1/r du/dx + d^2u/dx^2)
     * @param x x grid
     * @param t t grid. t[0] is offset. step size is t[i] - t[i-1].
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix to the file to identify different plots
     * @param t_end end time of calculation, -1 for complete t grid
     */
    void varying_grid_plot_r(VecNum_I &x, VecNum_I &t, NumType_I &kappa0, const std::string &file_appendix,
                             NumType t_end = -1);

    /**
     * Calculates the difference of the theory and the Crank-Nicolson method at the different time steps with non-equidistant grid.
     * @param steps NumVector of the different step counts, is casted into int
     * @param x x grid - only positive values!
     * @param t t grid. t[0] is offset. step size is t[i] - t[i-1].
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix to the file to identify different plots
     * @return vector with the maximum absolute difference at the time steps
     */
    VecNum
    varying_grid_error(VecNum_I &steps, VecNum_I &x, VecNum_I &t, NumType_I &kappa0, const std::string &file_appendix);

    /**
     * Calculates the difference of the theory and the numerical method at the different time steps for diffusion in a r coordinate with non-equidistant grid.
     * @param steps NumVector of the different step counts, is casted into int
     * @param x x grid
     * @param t t grid. t[0] is offset. step size is t[i] - t[i-1].
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix to the file to identify different plots
     * @return vector with the maximum absolute difference at the time steps
     */
    VecNum varying_grid_error_r(VecNum_I &steps, VecNum_I &x, VecNum_I &t, NumType_I &kappa0,
                                const std::string &file_appendix);
}

#endif //LECRP_ONEDIMDIFFUSION_HPP
