"""
Set the varibales and load and plot the templates.
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, integrate
import glob
import h5py
import time
import multiprocessing

np.set_printoptions(threshold=20, linewidth=150)

"""Some global parameters"""
mass_p = 938.272088  # <Mass of the Proton in MeV
light_speed_si = 299792458  # <c in m/s

"""Parameters in normal units"""
p0 = mass_p  # <p0 in MeV/c
b0 = 1.96879  # <b0 in (Mev/c)/Myr
kappa0 = 0.035  # <kappa0 in kpc^2/Myr
q0 = 1.42993e49  # <q0 in 1/(MeV/c)
delta_analytic = 0.63  # <delta in kappa in the analytic solution
n_analytic = -2.0  # <n in b in the analytic solution
gamma = 2.2  # <gamma in q

"""Variables for making dimensionless"""
tau = p0 / b0  # <tau in Myr, make t dimensionless
lamb = np.sqrt(kappa0 * p0 / b0)  # <lambda in kpc, make r dimensionless

"""dimensions of the Galaxy"""
# R = 10.  # <R in kpc
R = 20.  # <R in kpc
# R = 15.  # <R in kpc, same as Minh
L = 4.  # <L in kpc
h = 0.1  # <h in kpc

"""parameter for energy loss"""
beta_0 = 0.01  # <beta_0 in ionisation loss

"""parameter for ionisation losses"""
n_ion = 1.0  # <density in 1/cm^3, the ionisation rate is independent of density!
f_ion = 1.82e-7  # <factor ionisation losses in eV/s

"""parameter for ionisation rate"""
nu1 = 5. / 3.  # secondary ionisation
nu2 = 1.17  # helium abundance
nu3 = 1.43  # heavy CR and CR electrons


def convert_dat_to_hdf5(file='fEr_p.dat', savefile_appendix='_Minh',
                        log10_Emin=6.0, log10_Emax=10.0, dlogE=0.02, dlogE_scale=0.1,
                        log10_rmin=1.5, log10_rmax=4.0, dlogr=0.005,
                        log10_tmin=2.0, log10_tmax=8.0, dlogt=0.002, dlogt_scale=0.02):
    """
    Converts the .dat file of Minh's code into .hdf5 files of Florians's code.

    :param file: the .dat file
    :param savefile_appendix: the save file: 'greens_template'+appendix+'.hdf5'
    :param log10_Emin, log10_Emax, dlogE, dlogE_scale: parameter of template
    :param log10_rmin, log10_rmax, dlogr: parameter of template
    :param log10_tmin, log10_tmax, dlogt, dlogt_scale: parameter of template
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob(file)
    if not file_list:
        return -1

    u_in = np.loadtxt(file_list[0])

    Emin = pow(10, log10_Emin)
    Emax = pow(10, log10_Emax)  # eV
    rmin = pow(10, log10_rmin)
    rmax = pow(10, log10_rmax)  # pc
    tmin = pow(10, log10_tmin)
    tmax = pow(10, log10_tmax)  # yr

    Nt = int(np.log10(tmax / tmin) / dlogt) + 1
    Nr = int(np.log10(rmax / rmin) / dlogr) + 1
    NE = int(np.log10(Emax / Emin) / dlogE) + 1
    print('Nt=%i, Nr=%i, NE=%i' % (Nt, Nr, NE))

    scaleE = int(dlogE_scale / dlogE)
    scalet = int(dlogt_scale / dlogt)

    ekin = Emin * np.power(10, np.arange(0, NE, 1) * dlogE)  # eV
    r = rmin * np.power(10, np.arange(0, Nr, 1) * dlogr)  # pc
    t = tmin * np.power(10, np.arange(0, Nt, 1) * dlogt)  # yr

    ekin = ekin[::scaleE]
    t = t[::scalet]
    print('Size of t:%i, r:%i, E:%i' % (t.size, r.size, ekin.size))
    print(u_in, u_in.size)
    t_size = u_in.size / (ekin.size * r.size)
    if t_size != int(t_size):
        print("WARNING: THE FILE SEEMS TO BE CORRUPTED!!!  t.size=%i, t_size=%.2f" % (t.size, t_size))
        t = t[:int(t_size)]
        u_in = u_in[:t.size * ekin.size * r.size]
    elif t.size != int(t_size):
        print("WARNING: THE FILE SEEMS TO BE INCOMPLETE!!! t.size=%i, t_size=%i" % (t.size, t_size))
        t = t[:int(t_size)]
    print('tmax_in=%.2e yr, tmax_out=%.2e yr' % (tmax, t[-1]))
    u_in = u_in.reshape((t.size, ekin.size, r.size))

    ekin = ekin * 1e-6  # MeV
    p = np.sqrt((ekin + mass_p) ** 2 - mass_p ** 2)  # MeV/c
    log_p_out = np.log(p / p0)
    t_out = t * 1e-6  # Myr
    r_out = r * 1e-3  # kpc
    u_out = u_in  # ??
    for i, ek in enumerate(ekin):
        u_out[:, i, :] = u_in[:, i, :] * np.sqrt(ek ** 2 + 2. * ek * mass_p) / (ek + mass_p) * 2.938e70
        # u_out[:, i, :] = u_in[:, i, :] * np.sqrt(ek ** 2 + 2. * ek * mass_p) / (ek + mass_p) * np.power(3.0e18, 3)

    r_out /= lamb
    t_out /= tau
    u_out /= q0 / (lamb ** 3)
    log_p_out = np.concatenate(([0], log_p_out))  # Flo: My program dont use the first p step
    u_out = np.concatenate((np.zeros((t_out.size, 1, r_out.size)), u_out), axis=1)

    file = h5py.File('greens_template%s.hdf5' % savefile_appendix, 'w')
    file.create_dataset('t', data=t_out)
    file.create_dataset('log_p', data=log_p_out)
    file.create_dataset('r', data=r_out)
    file.create_dataset('u', data=u_out)

    return 0


def intensity_phan(ekin):
    """intensity by phan et al in (m^2 s sr MeV)^-1"""
    return 1.882e1 * pow(ekin, 0.129) * pow(1 + ekin / 624.5, -2.829)


def plot_template(file_appendix='', pos_t=[0.0], pos_p=[0.0], pos_r=[0.0], ylim=None, use_ekin=False,
                  do_plot_analytic=False, do_save_file=False):
    """
    Plot the .hdf template for different times t, impulse p, distances r.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param pos_t: The positions in time t to plot, list or array.
    :param pos_p: The positions in impulse p to plot, list or array.
    :param pos_r: The positions in distance r to plot, list or array.
    :param ylim: y limits of the plots.
    :param use_ekin: if True psi is plotted against ekin instead of p. pos_p is then also interpreted for ekin.
    :param do_plot_analytic: if True the analytic solution for energy loss everywhere is also plotted.
    :param do_save_file: if True the .csv files of the lines are saved.
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)
    ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

    # print(r)
    # print(p)
    # print(ekin, ekin[19])
    # print(t)

    index_t = []
    for x in pos_t:
        if x == -1:
            index_t += [-1]
        else:
            index_t += [np.abs(t - x).argmin()]
    index_p = []
    for x in pos_p:
        if x == -1:
            index_p += [-1]
        else:
            if use_ekin:
                index_p += [np.abs(ekin - x).argmin()]
            else:
                index_p += [np.abs(p - x).argmin()]
    index_r = []
    for x in pos_r:
        if x == -1:
            index_r += [-1]
        else:
            index_r += [np.abs(r - x).argmin()]

    if len(index_p) and len(index_r):
        plt.figure()
        for i_p in index_p:
            for i_r in index_r:
                color = next(plt.gca()._get_lines.prop_cycler)['color']
                label = 'p=%.1e MeV, r=%.1f kpc' % (p[i_p], r[i_r])
                if use_ekin:
                    label = '$E_{kin}$=%.1e MeV, r=%.1f kpc' % (ekin[i_p], r[i_r])
                plt.plot(t, u[:, i_p, i_r], label=label, color=color)
                if do_plot_analytic:
                    plt.plot(t, psi_analytic(r[i_r], p[i_p], t), '--', color=color)
                if do_save_file:
                    header = 'psi(t) at ' + label + '\nt[Myr] psi[kpc^-3 (MeV/c)^-1]'
                    np.savetxt('psi_t_p%i_r%i%s.csv' % (i_p, i_r, file_appendix), np.transpose([t, u[:, i_p, i_r]]),
                               header=header)
                    if do_plot_analytic:
                        np.savetxt('psi_analytic_t_p%i_r%i%s.csv' % (i_p, i_r, file_appendix),
                                   np.transpose([t, psi_analytic(r[i_r], p[i_p], t)]), header=header)
        if do_plot_analytic:
            plt.plot([], [], '--', label='analytic', color='grey')
        plt.xlabel('t [Myr]')
        plt.ylabel('$\\psi$(t, r, p) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template' + file_appendix + ', t axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_t%s.pdf' % file_appendix)

    if len(index_t) and len(index_r):
        x = p
        if use_ekin:
            x = ekin
        plt.figure()
        for i_t in index_t:
            for i_r in index_r:
                color = next(plt.gca()._get_lines.prop_cycler)['color']
                label = 't=%.1e Myr, r=%.1f kpc' % (t[i_t], r[i_r])
                plt.plot(x, u[i_t, :, i_r], label=label, color=color)
                if do_plot_analytic:
                    plt.plot(x, psi_analytic(r[i_r], p, t[i_t]), '--', color=color)
                if do_save_file:
                    header = 'psi(p) at ' + label + '\np[MeV] psi[kpc^-3 (MeV/c)^-1]'
                    if use_ekin:
                        header = 'psi(E_kin) at ' + label + '\nE_kin[MeV] psi[kpc^-3 (MeV/c)^-1]'
                    np.savetxt('psi_p_t%i_r%i%s.csv' % (i_t, i_r, file_appendix), np.transpose([x, u[i_t, :, i_r]]),
                               header=header)
                    if do_plot_analytic:
                        np.savetxt('psi_analytic_p_t%i_r%i%s.csv' % (i_t, i_r, file_appendix),
                                   np.transpose([x, psi_analytic(r[i_r], p, t[i_t])]), header=header)
        if do_plot_analytic:
            plt.plot([], [], '--', label='analytic', color='grey')
        plt.xlabel('p [MeV]')
        if use_ekin:
            plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template' + file_appendix + ', p axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_p%s.pdf' % file_appendix)

    if len(index_t) and len(index_p):
        plt.figure()
        for i_t in index_t:
            for i_p in index_p:
                color = next(plt.gca()._get_lines.prop_cycler)['color']
                label = 't=%.1e Myr, p=%.1e MeV' % (t[i_t], p[i_p])
                if use_ekin:
                    label = 't=%.1e Myr, $E_{kin}$=%.1e MeV' % (t[i_t], ekin[i_p])
                plt.plot(r, u[i_t, i_p, :], label=label, color=color)
                if do_plot_analytic:
                    plt.plot(r, psi_analytic(r, p[i_p], t[i_t]), '--', color=color)
                if do_save_file:
                    header = 'psi(r) at ' + label + '\nr[kpc] psi[kpc^-3 (MeV/c)^-1]'
                    np.savetxt('psi_r_t%i_p%i%s.csv' % (i_t, i_p, file_appendix), np.transpose([r, u[i_t, i_p, :]]),
                               header=header)
                    if do_plot_analytic:
                        np.savetxt('psi_analytic_r_t%i_p%i%s.csv' % (i_t, i_p, file_appendix),
                                   np.transpose([r, psi_analytic(r, p[i_p], t[i_t])]), header=header)
        if do_plot_analytic:
            plt.plot([], [], '--', label='analytic', color='grey')
        plt.xlabel('r [kpc]')
        plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template' + file_appendix + ', r axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_r%s.pdf' % file_appendix)
    return 0


def compare_greens_template(file_appendix_list, pos_t=[0.0], pos_p=[0.0], pos_r=[0.0], ylim=None, use_ekin=False,
                            do_plot_analytic=False, do_save_file=False):
    """
    Plot the .hdf template for different times t, impulse p, distances r.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param pos_t: The positions in time t to plot, list or array.
    :param pos_p: The positions in impulse p to plot, list or array.
    :param pos_r: The positions in distance r to plot, list or array.
    :param ylim: y limits of the plots.
    :param use_ekin: if True psi is plotted against ekin instead of p. pos_p is then also interpreted for ekin.
    :param do_plot_analytic: if True the analytic solution for energy loss everywhere is also plotted.
    :param do_save_file: if True the .csv files of the lines are saved.
    :return: 0, or -1 if file don't exist
    """
    file_list = []
    file_appendix_list_2 = []
    for i, file in enumerate(file_list):
        file_names[i] = file
        file_appendix += file
    for i, file_appendix in enumerate(file_appendix_list):
        file_name = glob.glob('greens_template%s.hdf5' % file_appendix)
        if file_name:
            file_list += [file_name[0]]
            file_appendix_list_2 += [file_appendix]
    file_appendix_list = file_appendix_list_2
    if not file_list:
        return -1

    t = [[] for i in file_list]
    log_p = [[] for i in file_list]
    r = [[] for i in file_list]
    u = [[] for i in file_list]
    p = [[] for i in file_list]
    ekin = [[] for i in file_list]
    for i, file_name in enumerate(file_list):
        file = h5py.File(file_name, 'r')
        t[i] = file['t'][...]
        log_p[i] = file['log_p'][...]
        r[i] = file['r'][...]
        u[i] = file['u'][...]
        log_p[i] = log_p[i][1:]  # cut of first point
        u[i] = u[i][:, 1:, :]

        r[i] = r[i] * lamb
        p[i] = np.exp(log_p[i]) * p0
        t[i] = t[i] * tau
        u[i] = u[i] * q0 / (lamb ** 3)
        ekin[i] = np.sqrt(p[i] ** 2 + mass_p ** 2) - mass_p

    index_t = [[] for i in file_list]
    index_p = [[] for i in file_list]
    index_r = [[] for i in file_list]
    for i, file in enumerate(file_list):
        index_t[i] = []
        for x in pos_t:
            if x == -1:
                index_t[i] += [-1]
            else:
                index_t[i] += [np.abs(t[i] - x).argmin()]
        index_p[i] = []
        for x in pos_p:
            if x == -1:
                index_p[i] += [-1]
            else:
                if use_ekin:
                    index_p[i] += [np.abs(ekin[i] - x).argmin()]
                else:
                    index_p[i] += [np.abs(p[i] - x).argmin()]
        index_r[i] = []
        for x in pos_r:
            if x == -1:
                index_r[i] += [-1]
            else:
                index_r[i] += [np.abs(r[i] - x).argmin()]

    linestyle = ['-', '--', '-.', '..']
    alpha = 0.8
    file_appendix = ''
    file_names = []
    for file in file_appendix_list:
        file_appendix += file
        file_names += [file[1:]]

    if len(pos_p) and len(pos_r):
        plt.figure()
        for i, file in enumerate(file_names):
            plt.gca().set_prop_cycle(None)
            for i_p in index_p[i]:
                for i_r in index_r[i]:
                    color = next(plt.gca()._get_lines.prop_cycler)['color']
                    label = '%s: p=%.1e MeV, r=%.1f kpc' % (file, p[i][i_p], r[i][i_r])
                    if use_ekin:
                        label = '%s: $E_{kin}$=%.1e MeV, r=%.1f kpc' % (file, ekin[i][i_p], r[i][i_r])
                    plt.plot(t[i], u[i][:, i_p, i_r], linestyle[i], label=label, color=color, alpha=alpha)
                    if do_plot_analytic:
                        plt.plot(t, psi_analytic(r[i_r], p[i_p], t), '--', color=color)
                    if do_save_file:
                        header = 'psi(t) at ' + label + '\nt[Myr] psi[kpc^-3 (MeV/c)^-1]'
                        np.savetxt('psi_t_p%i_r%i_%s.csv' % (i_p, i_r, file), np.transpose([t[i], u[i][:, i_p, i_r]]),
                                   header=header)
                        if do_plot_analytic:
                            np.savetxt('psi_analytic_t_p%i_r%i%s.csv' % (i_p, i_r, file_appendix),
                                       np.transpose([t[i], psi_analytic(r[i][i_r], p[i][i_p], t[i])]), header=header)
        if do_plot_analytic:
            plt.plot([], [], '--', label='analytic', color='grey')
        plt.xlabel('t [Myr]')
        plt.ylabel('$\\psi$(t, r, p) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template' + file_appendix + ', t axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_t%s.pdf' % file_appendix)

    if len(index_t) and len(index_r):
        x = p
        if use_ekin:
            x = ekin
        plt.figure()
        for i, file in enumerate(file_names):
            plt.gca().set_prop_cycle(None)
            for i_t in index_t[i]:
                for i_r in index_r[i]:
                    color = next(plt.gca()._get_lines.prop_cycler)['color']
                    label = '%s: t=%.1e Myr, r=%.1f kpc' % (file, t[i][i_t], r[i][i_r])
                    plt.plot(x[i], u[i][i_t, :, i_r], linestyle[i], label=label, color=color, alpha=alpha)
                    if do_plot_analytic:
                        plt.plot(x[i], psi_analytic(r[i][i_r], p[i], t[i][i_t]), '--', color=color)
                    if do_save_file:
                        header = 'psi(p) at ' + label + '\np[MeV] psi[kpc^-3 (MeV/c)^-1]'
                        if use_ekin:
                            header = 'psi(E_kin) at ' + label + '\nE_kin[MeV] psi[kpc^-3 (MeV/c)^-1]'
                        np.savetxt('psi_p_t%i_r%i_%s.csv' % (i_t, i_r, file), np.transpose([x[i], u[i][i_t, :, i_r]]),
                                   header=header)
                        if do_plot_analytic:
                            np.savetxt('psi_analytic_p_t%i_r%i%s.csv' % (i_t, i_r, file_appendix),
                                       np.transpose([x[i], psi_analytic(r[i][i_r], p[i], t[i][i_t])]), header=header)
        if do_plot_analytic:
            plt.plot([], [], '--', label='analytic', color='grey')
        plt.xlabel('p [MeV]')
        if use_ekin:
            plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template' + file_appendix + ', p axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_p%s.pdf' % file_appendix)

    if len(index_t) and len(index_p):
        plt.figure()
        for i, file in enumerate(file_names):
            plt.gca().set_prop_cycle(None)
            for i_t in index_t[i]:
                for i_p in index_p[i]:
                    color = next(plt.gca()._get_lines.prop_cycler)['color']
                    label = '%s: t=%.1e Myr, p=%.1e MeV' % (file, t[i][i_t], p[i][i_p])
                    if use_ekin:
                        label = '%s: t=%.1e Myr, $E_{kin}$=%.1e MeV' % (file, t[i][i_t], ekin[i][i_p])
                    plt.plot(r[i], u[i][i_t, i_p, :], linestyle[i], label=label, color=color, alpha=alpha)
                    if do_plot_analytic:
                        plt.plot(r[i], psi_analytic(r[i], p[i][i_p], t[i][i_t]), '--', color=color)
                    if do_save_file:
                        header = 'psi(r) at ' + label + '\nr[kpc] psi[kpc^-3 (MeV/c)^-1]'
                        np.savetxt('psi_r_t%i_p%i_%s.csv' % (i_t, i_p, file), np.transpose([r[i], u[i][i_t, i_p, :]]),
                                   header=header)
                        if do_plot_analytic:
                            np.savetxt('psi_analytic_r_t%i_p%i%s.csv' % (i_t, i_p, file_appendix),
                                       np.transpose([r[i], psi_analytic(r[i], p[i][i_p], t[i][i_t])]), header=header)
        if do_plot_analytic:
            plt.plot([], [], '--', label='analytic', color='grey')
        plt.xlabel('r [kpc]')
        plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template' + file_appendix + ', r axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_r%s.pdf' % file_appendix)
    return 0


def compare_greens_template_old(file_appendix_1, file_appendix_2, pos_t=[0.0], pos_p=[0.0], pos_r=[0.0], ylim=None):
    """
    Old method, use compare_greens_template instead.
    Compare two .hdf5 templates for different times t, impulse p, distances r.

    :param file_appendix_1, file_appendix_2: file identifier for the templates,
     the file 'greens_template'+file_appendix'.hdf5' is used if it exists.
    :param pos_t: The positions in time t to plot, list or array.
    :param pos_p: The positions in impulse p to plot, list or array.
    :param pos_r: The positions in distance r to plot, list or array.
    :param ylim: y limits of the plots.
    :return: 0, or -1 if file don't exist
    """
    file_list_1 = glob.glob('greens_template%s.hdf5' % file_appendix_1)
    file_list_2 = glob.glob('greens_template%s.hdf5' % file_appendix_2)
    if not file_list_1 or not file_list_2:
        return -1
    file = h5py.File(file_list_1[0], 'r')
    t_1 = file['t'][...]
    log_p_1 = file['log_p'][...]
    r_1 = file['r'][...]
    u_1 = file['u'][...]
    log_p_1 = log_p_1[1:]  # cut of first point
    u_1 = u_1[:, 1:, :]

    r_1 = r_1 * lamb
    p_1 = np.exp(log_p_1) * p0
    t_1 = t_1 * tau
    u_1 = u_1 * q0 / (lamb ** 3)

    file = h5py.File(file_list_2[0], 'r')
    t_2 = file['t'][...]
    log_p_2 = file['log_p'][...]
    r_2 = file['r'][...]
    u_2 = file['u'][...]
    log_p_2 = log_p_2[1:]  # cut of first point
    u_2 = u_2[:, 1:, :]

    r_2 = r_2 * lamb
    p_2 = np.exp(log_p_2) * p0
    t_2 = t_2 * tau
    u_2 = u_2 * q0 / (lamb ** 3)

    index_t_1 = []
    index_t_2 = []
    for x in pos_t:
        if x == -1:
            index_t_1 += [-1]
            index_t_2 += [-1]
        else:
            index_t_1 += [np.abs(t_1 - x).argmin()]
            index_t_2 += [np.abs(t_2 - x).argmin()]
    index_p_1 = []
    index_p_2 = []
    for x in pos_p:
        if x == -1:
            index_p_1 += [-1]
            index_p_2 += [-1]
        else:
            index_p_1 += [np.abs(p_1 - x).argmin()]
            index_p_2 += [np.abs(p_2 - x).argmin()]
    index_r_1 = []
    index_r_2 = []
    for x in pos_r:
        if x == -1:
            index_r_1 += [-1]
            index_r_2 += [-1]
        else:
            index_r_1 += [np.abs(r_1 - x).argmin()]
            index_r_2 += [np.abs(r_2 - x).argmin()]

    if len(pos_p) and len(pos_r):
        plt.figure()
        for i_p in index_p_1:
            for i_r in index_r_1:
                plt.plot(t_1[:], u_1[:, i_p, i_r], '-', alpha=0.5,
                         label='%s: p=%.1e MeV, r=%.1f kpc' % (file_appendix_1, p_1[i_p], r_1[i_r]))
        plt.gca().set_prop_cycle(None)
        for i_p in index_p_2:
            for i_r in index_r_2:
                plt.plot(t_2[:], u_2[:, i_p, i_r], '--',
                         label='%s: p=%.1e MeV, r=%.1f kpc' % (file_appendix_2, p_2[i_p], r_2[i_r]))
        plt.xlabel('t [Myr]')
        plt.ylabel('$\\psi$(t, r, p) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template, t axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.tight_layout()
        plt.savefig('CRDiffCompareTemplate_t_%s_%s.pdf' % (file_appendix_1, file_appendix_2))

    if len(pos_t) and len(pos_r):
        plt.figure()
        for i_t in index_t_1:
            for i_r in index_r_1:
                plt.plot(p_1, u_1[i_t, :, i_r], '-', alpha=0.5,
                         label='%s: t=%.1e Myr, r=%.1f kpc' % (file_appendix_1, t_1[i_t], r_1[i_r]))
        plt.gca().set_prop_cycle(None)
        for i_t in index_t_2:
            for i_r in index_r_2:
                plt.plot(p_2, u_2[i_t, :, i_r], '--',
                         label='%s: t=%.1e Myr, r=%.1f kpc' % (file_appendix_2, t_2[i_t], r_2[i_r]))
        plt.xlabel('p [MeV]')
        plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template, p axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffCompareTemplate_p_%s_%s.pdf' % (file_appendix_1, file_appendix_2))

    if len(pos_t) and len(pos_p):
        plt.figure()
        for i_t in index_t_1:
            for i_p in index_p_1:
                plt.plot(r_1, u_1[i_t, i_p, :], '-', alpha=0.5,
                         label='%s: t=%.1e Myr, p=%.1e MeV' % (file_appendix_1, t_1[i_t], p_1[i_p]))
        plt.gca().set_prop_cycle(None)
        for i_t in index_t_2:
            for i_p in index_p_2:
                plt.plot(r_2, u_2[i_t, i_p, :], '--',
                         label='%s: t=%.1e Myr, p=%.1e MeV' % (file_appendix_2, t_2[i_t], p_2[i_p]))
        plt.xlabel('r [kpc]')
        plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.title('CR Diffusion Template, r axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(ylim)
        plt.tight_layout()
        plt.savefig('CRDiffCompareTemplate_r_%s_%s.pdf' % (file_appendix_1, file_appendix_2))
    return 0


def plot_integrated_p(file_appendix='', pos_t=[0.0]):
    """
    Plot the .hdf template for different times t integrated in p.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param pos_t: The positions in time t to plot, list or array.
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)
    ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

    index_t = []
    for x in pos_t:
        if x == -1:
            index_t += [-1]
        else:
            index_t += [np.abs(t - x).argmin()]

    plt.figure()
    for i_t in index_t:
        # u_integrated = u[i_t, 19, :]
        """Integrate in p via summation"""
        dp = p[1:] - p[:-1]
        dp = np.append(dp, [0])
        u_integrated = np.tensordot(u[i_t, :, :], dp, axes=(0, 0))  # Sum in p

        plt.plot(r, u_integrated, label='t=%.1e Myr' % (t[i_t]))
        np.savetxt('psi_integrated_t%i%s.csv' % (i_t, file_appendix), np.transpose([r, u_integrated]),
                   header='psi(r) at t = %.2e Myr' % (t[i_t])
                          + '\n r[kpc] psi[kpc^-3 (MeV/c)^-1]')
    plt.xlabel('r [kpc]')
    plt.ylabel('$\\psi$(t, p, r) [$kpc^{-3} (MeV/c)^{-1}$]')
    plt.title('CR Diffusion Integrated, r axis')
    plt.legend()
    plt.ylim(1e35, 1e52)
    plt.xscale('log')
    plt.yscale('log')
    plt.tight_layout()
    plt.savefig('CRDiffIntegrated%s.pdf' % file_appendix)
    return 0


def plot_time_development(file_appendix='', r0_array=[0.1], p0_array=[1, 100, 10000], use_analytic=True,
                          plot_free_greens_function=False):
    """
    Plot the time development of the CR density.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param r0_array: The distances r_0 to be plotted (array)
    :param p0_array: The impulse p to be plotted (array)
    :param use_analytic: if True analytic calculation is plotted too (only if template file+'_analytic' exists)
    :param plot_free_greens_function: True if free Green's function should be plotted too
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if file_appendix == '_3':
        file_list = glob.glob('greens_template.hdf5')
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    t = t * tau
    u = u * q0 / (lamb ** 3)

    if use_analytic:
        file_list = glob.glob('greens_template_analytic.hdf5')
        if not file_list:
            use_analytic = False
    if use_analytic:
        file = h5py.File(file_list[0], 'r')
        r_analytic = file['r'][...]
        log_p_analytic = file['log_p'][...]
        t_analytic = file['t'][...]
        u_analytic = file['u'][...]

        t_analytic = t_analytic * tau
        u_analytic = u_analytic * q0 / (lamb ** 3)

    plt.figure()
    for r0 in r0_array:
        index_r0 = np.abs(r - r0 / lamb).argmin()
        for p_val in p0_array:
            index_p0 = np.abs(log_p - np.log(p_val / p0)).argmin()
            p = p0 * np.exp(log_p[index_p0])
            y = (p / p0) ** gamma * t ** (3 / 2) * u[:, index_p0, index_r0]
            label = 'r=%.1fkpc, p=%.1eMeV/c' % (r[index_r0] * lamb, p)
            plt.plot(t, y, '-', label=label)

    if use_analytic:
        plt.gca().set_prop_cycle(None)
        label = 'analytic'
        if file_appendix == '_3':
            label = 'finer grid'
        for r0 in r0_array:
            index_r0 = np.abs(r_analytic - r0 / lamb).argmin()
            for p_val in p0_array:
                index_p0 = np.abs(log_p_analytic - np.log(p_val / p0)).argmin()
                p = p0 * np.exp(log_p_analytic[index_p0])
                y = (p / p0) ** gamma * t_analytic ** (3 / 2) * u_analytic[:, index_p0, index_r0]
                plt.plot(t_analytic, y, '--', label=label)
                label = None

    if plot_free_greens_function:
        plt.gca().set_prop_cycle(None)
        label = "free Green`s function"
        for r0 in r0_array:
            index_r0 = np.abs(r - r0 / lamb).argmin()
            for p_val in p0_array:
                index_p = np.abs(log_p - np.log(p_val / p0)).argmin()
                kappa_val = kappa0 * np.exp(delta_analytic * log_p[index_p])
                """psi = e^(- r^2 / (4 t kappa)) / (4 pi t kappa)^(3/2) * q(p)"""
                y = np.exp(-(r[index_r0] * lamb) ** 2 / (4. * t * kappa_val)) / (
                        4. * np.pi * kappa_val) ** (3. / 2.) * q0
                plt.plot(t, y, ':', label=label)
                label = None

    plt.xlabel('$t-t_0$ [Myr]')
    plt.ylabel(r'$(\frac{p}{p_0})^{\Gamma} (\frac{t-t_0}{Myr})^{3/2} \Psi(r,p,t)$ [$kpc^{-3} (MeV/c)^{-1}$]')
    # plt.title('Time development')
    plt.legend()
    # box = plt.gca().get_position()
    # plt.gca().set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(bottom=1e40, top=1e50)
    if plot_free_greens_function:
        plt.ylim(bottom=1e40, top=1e52)
    plt.grid()
    plt.tight_layout()
    plt.savefig('CRDiffTimeDevelopment%s.pdf' % file_appendix)
    return 0


def plot_spectrum(file_appendix='', t0_array=[0.0], r0_array=[0.0], ylim_bottom=1e35, plot_power_law=False):
    """
    Plot the spectrum in one .hdf template for different times t0 and distances r0.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t0_array: The times t_0 to be plotted (array)
    :param r0_array: The distances r_0 to be plotted (array)
    :param ylim_bottom: The bottom ylim, spectra below this value are not plotted. 0 if it should be automatic
    :param plot_power_law: if True the power low dependencies of the steady state solution ae plotted too.
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)
    ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

    index_t = []
    for x in t0_array:
        if x == -1:
            index_t += [-1]
        else:
            index_t += [np.abs(t - x).argmin()]
    index_r = []
    for x in r0_array:
        if x == -1:
            index_r += [-1]
        else:
            index_r += [np.abs(r - x).argmin()]

    # x_axis = ekin
    x_axis = p
    if plot_power_law:
        index_p0 = np.argmin(np.abs(p - p0))
        u0 = u[:, index_p0, :]  # u[t, r] at p0
        # u0 = u0[index_t[0], index_r[0]]
        u0 = u0[index_t[0], :] * 3e4  # u[r] at t
        i_start = 0
        i_stop = 21
        print(index_p0)
        print(u0[i_start:i_stop])
        print(r[i_start:i_stop])
        # u0 = np.mean(2 * np.pi * r * u0)
        u0 = 2 * np.pi * np.sum(r[i_start:i_stop] * u0[i_start:i_stop])
        print('u0\t', u0)
        A = 3e4 / (np.pi * (R) ** 2 * h)
        print('A\t', A)
        fac = u0 * b0 / (A * q0 * p0)
        print('fac\t', fac)
        print('fac2\t', A * q0 * p0 / b0 * 1 / (gamma - 1))
        # fac = 1
        print('parameter', fac * (gamma - 1) - 1, 1 / (1 - gamma))
        p1 = p0 * np.power(fac * (gamma - 1) - 1, 1 / (1 - gamma))
        print('p1\t', p1)

    plt.figure()
    linestyles = ['-', '--', ':']
    for i, i_r in enumerate(index_r):
        plt.gca().set_prop_cycle(None)
        for i_t in index_t:
            y = u[i_t, :, i_r]
            if i == 0:
                label = 't=%.1e Myr' % t[i_t]
            if len(index_r) == 1:
                label += ', r=%.1f kpc' % r[i_r]
            if np.max(y) > ylim_bottom or True:
                obj = plt.plot(x_axis, y, linestyles[i], label=label)
                label = None
                if plot_power_law:
                    y_power_law_1 = y[0] * np.power(p / p[0], -n_analytic - gamma + 1)
                    # y_power_law_1 = y[0] * np.power(p / p[0], -n_analytic)
                    plt.plot(x_axis, y_power_law_1, '--', color=obj[-1].get_color())
                    y_power_law_2 = y[-1] * np.power(p / p[-1], -gamma - delta_analytic)
                    plt.plot(x_axis, y_power_law_2, '-.', color=obj[-1].get_color())
    if len(index_r) > 1:
        for i, i_r in enumerate(index_r):
            plt.plot(0, 0, linestyles[i], color='gray', label='r=%.1f kpc' % r[i_r])

    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    plt.ylabel('$\\psi$(p) [$kpc^{-3} (MeV/c)^{-1}$]')
    # plt.title('Spectrum')
    plt.legend(ncol=2)
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    # plt.ylim(1e35, 1e53)
    if ylim_bottom > 0:
        plt.ylim(bottom=ylim_bottom)
    plt.ylim(top=2e52)
    plt.tight_layout()
    if plot_power_law:
        plt.savefig('CRDiffSpectrum_power_law%s.pdf' % file_appendix)
    else:
        plt.savefig('CRDiffSpectrum%s.pdf' % file_appendix)
    return 0


def calculate_slope(file_appendix=''):
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)

    val = (u[-1, 1, 0] - u[-1, 0, 0]) / (p[1] - p[0])
    val = np.log10(u[-1, 1, 0] / u[-1, 0, 0]) / np.log10(p[1] / p[0])
    print('Slope of %s at t=%.2e, r=%.2e, p0=%.2e, p1=%.2e: ' % (file_appendix, t[-1], r[0], p[0], p[1]), val)

    return val


"""Part for the analytic solution with energy loss everywhere"""


def p_t0(p, t):
    return p0 * np.power(b0 * t * (1 - n_analytic) / p0 + np.power(p / p0, 1 - n_analytic), 1. / (1 - n_analytic))


def l_2(p, pt0):
    return 4 * kappa0 / b0 * p0 / (delta_analytic - n_analytic + 1) * (
            np.power(pt0 / p0, delta_analytic - n_analytic + 1) - np.power(p / p0, delta_analytic - n_analytic + 1))


def psi_analytic(r, p, t):
    pt0 = p_t0(p, t)
    l2 = l_2(p, pt0)
    b_p = b0 * np.power(p / p0, n_analytic)
    b_pt0 = b0 * np.power(pt0 / p0, n_analytic)
    Q_pt0 = q0 * np.power(pt0 / p0, -gamma)
    Z = 0
    for n in range(-100, 100):
        Z += (-1) ** n * np.exp(- 4 * n ** 2 * L ** 2 / l2)
    return np.abs(b_pt0 / b_p) * Q_pt0 / np.power(np.pi * l2, 3. / 2) * np.exp(- r ** 2 / l2) * Z
