//
// Created by flosc on 25.05.2020.
//

#include "CRDiffusion.hpp"
#include "NDNumerics.hpp"
#include "parameters.hpp"
#include <cmath>
#include <iostream>
#include <numeric>
#include <hdf5.h>
#include <omp.h>

using namespace std;
using namespace plot;
#define IND(j) (SizeType) (j), subvector_index, index
#define USE_CALCULATE_STEPS 1 ///< 1:power law, 2:real coefficients
#define USE_INITIAL_CONDITION 1 ///< 1:gauss, 2:cylinder with zmin and rmin, 3:cylinder like Minh

///Parameters for diffusion and energy loss coefficients
const NumType mass_p = MASS_P / P0; ///<dimensionless proton mass
///Parameters for Energy loss
const NumType n_WNM = 0.5, n_WIM = 0.5, n_HIM = 0.006; ///<density in 1/cm^3
const NumType f_WNM = 0.25, f_WIM = 0.25, f_HIM = 0.5; ///<filling factor
const NumType f_ion = 5.73955 / B0, f_coulomb = 9.77616 / B0; ///<factor ionisation/coulomb losses, dimensionless
const NumType beta_0 = 0.01; ///<beta_0 in ionisation loss
///x_m = 0.0286*sqrt(Te/2*10^6 K) in coulomb loss
const NumType xm_WNM = 0.0286 * sqrt(4e-2), xm_WIM = 0.0286 * sqrt(4e-2), xm_HIM = 0.0286 * sqrt(0.5);

NumType CRDiff::energy_loss_rate(NumType_I &log_p) {
    ///beta = (p/m) / sqrt(1 + (p/m)^2)
    NumType beta = (exp(log_p) / mass_p) / sqrt(1.0 + exp(2.0 * log_p) / (mass_p * mass_p)); ///<beta = v/c
    ///dE/dt = f_WNM * dE/dt_WNM + f_WIM * dE/dt_WIM + f_HIM * dE/dt_HIM
    ///dE/dt_WNM = dE/dt_ion; dE/dt_WIM, dE/dt_HIM = dE/dt_coulomb
    ///dp/dt = dp/dE dE/dt = sqrt(1 + (m/p)^2) dE/dt
    return exp(-log_p) * sqrt(1.0 + mass_p * mass_p * exp(-2.0 * log_p)) * ( ///u is scaled with exp(log_p)
            f_WNM * (
                    -f_ion * n_WNM * (1.0 + 0.0185 * log(beta)) * 2.0 * beta * beta /
                    (pow(beta_0, 3) + 2.0 * pow(beta, 3)))
            + f_WIM * (
                    -f_coulomb * n_WIM * beta * beta / (pow(xm_WIM, 3) + pow(beta, 3)))
            + f_HIM * (
                    -f_coulomb * n_HIM * beta * beta / (pow(xm_HIM, 3) + pow(beta, 3))));
}

void CRDiff::diffusion_step_r_z(VecND &un, VecNum_I &dr, VecNum_I &dz, NumType_I &dt, NumType_I &delta, VecNum_I &r,
                                VecNum_I &log_p) {
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, r, log_p, dr, dz, dt, delta) firstprivate(index)
    for (SizeType i_log_p = 0; i_log_p < un.get_size(0); i_log_p++) {
        index[0] = i_log_p;
        NumType kappa_2 = exp(delta * log_p[i_log_p]) / 2.0; ///kappa/2, because needed in 1/2 for crank_nicolson

        ///Step in z
        VecNum a_z(un.get_size(1)); ///For implicit method
        VecNum b_z(un.get_size(1));
        VecNum c_z(un.get_size(1));
        for (SizeType j = 1; j < a_z.size() - 1; j++) {
            NumType alpha_j = kappa_2 * dt * 2. / (dz[j] + dz[j - 1]);
            a_z[j] = -alpha_j / dz[j - 1];
            b_z[j] = 1 + alpha_j / dz[j] + alpha_j / dz[j - 1];
            c_z[j] = -alpha_j / dz[j];
        }
        b_z.front() = 1 + 2. * kappa_2 * dt / (dz[0] * dz[0]); ///symmetric boundary start
        c_z.front() = -2. * kappa_2 * dt / (dz[0] * dz[0]);
        a_z.back() = 0; ///constant boundary end
        b_z.back() = 1;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
            NDNumerics::diffusion_ftcs_index(un, 1, index, dz, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 1, index, a_z, b_z, c_z);
        }

        ///Step in r
        VecNum a_r(un.get_size(2));
        VecNum b_r(un.get_size(2));
        VecNum c_r(un.get_size(2));
        for (SizeType j = 1; j < a_r.size() - 1; j++) {
            NumType alpha_j = kappa_2 * dt * 2. / (dr[j] + dr[j - 1]);
            a_r[j] = -alpha_j / dr[j - 1];
            b_r[j] = 1 + alpha_j / dr[j] + alpha_j / dr[j - 1];
            c_r[j] = -alpha_j / dr[j];
        }
        b_r.front() = 1 + 2. * kappa_2 * dt / (dr[0] * dr[0]); ///boundary start
        c_r.front() = -2. * kappa_2 * dt / (dr[0] * dr[0]);
        a_r.back() = 0; ///constant boundary end
        b_r.back() = 1;
        ///du/dt = kappa/r du/dr part
        auto kappa_r_func = [&kappa_2](NumType_I &x) {
            return -kappa_2 * 2.0 / x;
        };
        VecNum kappa_r = vec_apply(r, kappa_r_func);
        for (SizeType i_z = 0; i_z < un.get_size(1); i_z++) {
            index[1] = i_z;
            NDNumerics::diffusion_ftcs_index(un, 2, index, dr, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 2, index, a_r, b_r, c_r);
            NDNumerics::semi_lagrangian_index(un, 2, index, dr, dt, kappa_r);
        }
    }
}

void CRDiff::diffusion_step_r_z_2(VecND &un, VecNum_I &dr, VecNum_I &dz, NumType_I &dt, NumType_I &delta, VecNum_I &r,
                                  VecNum_I &log_p) {
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, r, log_p, dr, dz, dt, delta) firstprivate(index)
    for (SizeType i_log_p = 0; i_log_p < un.get_size(0); i_log_p++) {
        index[0] = i_log_p;
        ///beta = (p/m) / sqrt(1 + (p/m)^2)
        NumType beta = (exp(log_p[i_log_p]) / mass_p) /
                       sqrt(1.0 + exp(2.0 * log_p[i_log_p]) / (mass_p * mass_p)); ///<beta = v/c
        ///kappa = beta * (1 + p/m)^delta
        NumType kappa_2 = beta * pow(1 + exp(log_p[i_log_p]) / mass_p, delta) / 2.0; ///<kappa/2

        ///Step in z
        VecNum a_z(un.get_size(1)); ///For implicit method
        VecNum b_z(un.get_size(1));
        VecNum c_z(un.get_size(1));
        for (SizeType j = 1; j < a_z.size() - 1; j++) {
            NumType alpha_j = kappa_2 * dt * 2. / (dz[j] + dz[j - 1]);
            a_z[j] = -alpha_j / dz[j - 1];
            b_z[j] = 1 + alpha_j / dz[j] + alpha_j / dz[j - 1];
            c_z[j] = -alpha_j / dz[j];
        }
        b_z.front() = 1 + 2. * kappa_2 * dt / (dz[0] * dz[0]); ///symmetric boundary start
        c_z.front() = -2. * kappa_2 * dt / (dz[0] * dz[0]);
        a_z.back() = 0; ///constant boundary end
        b_z.back() = 1;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
            NDNumerics::diffusion_ftcs_index(un, 1, index, dz, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 1, index, a_z, b_z, c_z);
        }

        ///Step in r
        VecNum a_r(un.get_size(2));
        VecNum b_r(un.get_size(2));
        VecNum c_r(un.get_size(2));
        for (SizeType j = 1; j < a_r.size() - 1; j++) {
            NumType alpha_j = kappa_2 * dt * 2. / (dr[j] + dr[j - 1]);
            a_r[j] = -alpha_j / dr[j - 1];
            b_r[j] = 1 + alpha_j / dr[j] + alpha_j / dr[j - 1];
            c_r[j] = -alpha_j / dr[j];
        }
        b_r.front() = 1 + 2. * kappa_2 * dt / (dr[0] * dr[0]); ///boundary start
        c_r.front() = -2. * kappa_2 * dt / (dr[0] * dr[0]);
        a_r.back() = 0; ///constant boundary end
        b_r.back() = 1;
        ///du/dt = kappa/r du/dr part
        auto kappa_r_func = [&kappa_2](NumType_I &x) {
            return -kappa_2 * 2.0 / x;
        };
        VecNum kappa_r = vec_apply(r, kappa_r_func);
        for (SizeType i_z = 0; i_z < un.get_size(1); i_z++) {
            index[1] = i_z;
            NDNumerics::diffusion_ftcs_index(un, 2, index, dr, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 2, index, a_r, b_r, c_r);
            NDNumerics::semi_lagrangian_index(un, 2, index, dr, dt, kappa_r);
        }
    }
}

void CRDiff::calculate_step_p(VecND &un, NumType_I &dlog_p, NumType_I &dt, NumType_I &power_n, VecNum_I &log_p, SizeType_I &end_z) {
    ///Step in p

    ///Velocity is independent of r,z,phi
    ///v = -exp((n-1)*log_p)
    auto velocity = [&power_n](NumType_I &x) {
        return -exp((power_n - 1) * x);
    };
    VecNum v = vec_apply(log_p, velocity);

    const NumType beta = 0.5; ///grad of implicity
    VecNum a1(v.size()); ///Matrices for explicit scheme
    VecNum b1(v.size());
    VecNum c1(v.size());
    VecNum a2(v.size()); ///Matrices for implicit scheme
    VecNum b2(v.size());
    VecNum c2(v.size());
    NDNumerics::advection_matrix(a1, b1, c1, v, dlog_p, dt * (1. - beta));
    NDNumerics::advection_matrix(a2, b2, c2, v, dlog_p, -dt * beta);

    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, a1, b1, c1, a2, b2, c2, end_z) firstprivate(index)
    for (SizeType i_z = 0; i_z <= end_z; i_z++) { ///Energy loss only in disk
        index[1] = i_z;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
            NDNumerics::matrix_multiplication_explicit(un, 0, index, a1, b1, c1);
            NDNumerics::matrix_multiplication_implicit(un, 0, index, a2, b2, c2);
        }
    }

    ///Old method
//    IndexType index = {0, 0, 0};
//#pragma omp parallel for default(none) shared(un, log_p, v, dlog_p, dt, end_z) firstprivate(index)
//    for (SizeType i_z = 0; i_z <= end_z; i_z++) { ///Energy loss only in disk
//        index[1] = i_z;
//        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
//            index[2] = i_r;
////            NDNumerics::semi_lagrangian_2_index(un, 0, index, dlog_p, dt, v, 2);
////            NDNumerics::upwind_index(un, 0, index, dlog_p, dt, v);
////            NDNumerics::mpdata_2_index(un, 0, index, dlog_p, dt, v);
//            NDNumerics::mpdata_index(un, 0, index, dlog_p, dt, v);
//        }
//    }
}

void CRDiff::calculate_step_p_2(VecND &un, NumType_I &dlog_p, NumType_I &dt, VecNum_I &log_p, SizeType_I &end_z) {
    VecNum v = vec_apply(log_p, energy_loss_rate);

    const NumType beta = 0.5; ///grad of implicity
    VecNum a1(v.size()); ///Matrices for explicit scheme
    VecNum b1(v.size());
    VecNum c1(v.size());
    VecNum a2(v.size()); ///Matrices for implicit scheme
    VecNum b2(v.size());
    VecNum c2(v.size());
    NDNumerics::advection_matrix(a1, b1, c1, v, dlog_p, dt * (1. - beta));
    NDNumerics::advection_matrix(a2, b2, c2, v, dlog_p, -dt * beta);

    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, a1, b1, c1, a2, b2, c2, end_z) firstprivate(index)
    for (SizeType i_z = 0; i_z <= end_z; i_z++) { ///Energy loss only in disk
        index[1] = i_z;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
            NDNumerics::matrix_multiplication_explicit(un, 0, index, a1, b1, c1);
            NDNumerics::matrix_multiplication_implicit(un, 0, index, a2, b2, c2);
        }
    }

    ///Old method
//    IndexType index = {0, 0, 0};
//#pragma omp parallel for default(none) shared(un, log_p, v, dlog_p, dt, end_z) firstprivate(index)
//    for (SizeType i_z = 0; i_z <= end_z; i_z++) { ///Energy loss only in disk
//        index[1] = i_z;
//        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
//            index[2] = i_r;
//            NDNumerics::mpdata_index(un, 0, index, dlog_p, dt, v);
//        }
//    }
}


void CRDiff::calculate_u0(VecND &un, NumType_I &gamma, VecNum_I &r, VecNum_I &z, VecNum_I &log_p, NumType_I &epsilon) {
    ///injection = dirac_delta(x) p^-gamma
    ///Replace driac_delta(x) with 1 / sqrt(2 pi epsilon) dirac_delta(r) dirac_delta(z)
    ///with epsilon = 2 * dt * offset in comparison with solution to TwoDimDiff equation
    function<NumType(VecNum_I &)> f0 = [&gamma, &epsilon](VecNum_I &val) {
        return 1.0 / sqrt(epsilon * 2.0 * M_PI)
               * dirac_delta(val[1], epsilon) * dirac_delta(val[2], epsilon) ///dirac_delta(x)
               * exp((-gamma + 1.0) * val[0]); ///Injection Spectrum and scale function with exp(log_p)
    };
    un.apply({log_p, z, r}, f0);

    ///Boundary in z and r
    auto set_boundaries = [](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        u.at(-1, subvector_index, index) = 0;
    };
    un.apply_subvector(set_boundaries, 1);
    un.apply_subvector(set_boundaries, 2);
}


void CRDiff::calculate_u0_minh(VecND &un, NumType_I &gamma, VecNum_I &r, VecNum_I &z, VecNum_I &log_p,
                               NumType_I &r_min, NumType_I &z_min) {
    ///injection = dirac_delta(x) p^-gamma
    function<NumType(VecNum_I &)> f0 = [&gamma, &r_min, &z_min](VecNum_I &val) {
        if (val[1] > z_min or val[2] > r_min) {
            return 0.0;
        } else {
            return exp((-gamma + 1.0) * val[0]) /
                   (2.0 * z_min * M_PI * r_min * r_min); ///Injection Spectrum and scale function with exp(log_p)
        }
    };

    un.apply({log_p, z, r}, f0);

    ///Boundary in z and r
    auto set_boundaries = [](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        u.at(-1, subvector_index, index) = 0;
    };
    un.apply_subvector(set_boundaries, 1);
    un.apply_subvector(set_boundaries, 2);
}

void CRDiff::plot_energy_loss_rate(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &dt,
                                   const std::string &file_appendix) {
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    VecNum v = vec_apply(log_p, energy_loss_rate);

    ///Give out the energy loss rate, so the step size can be fit to the maximum energy loss rate.
    cout << "energy loss rate:" << endl;
    NumType v_max = vec_max(vec_apply(v, abs));
    cout << " maximum energy loss: " << v_max << endl;
    if (dt > 0) {
        NumType alpha = v_max * dt / dlog_p;
        cout << " alpha = max(abs(v)) * dt / dx = " << alpha << endl;
        if (alpha <= 1) {
            cout << " mpdata is stable with this parameters." << endl;
        } else {
            cout << " mpdata is not stable with this parameters." << endl;
        }
        cout << " minimum required dt = " << dlog_p / v_max << " = " << dlog_p / v_max * TAU << " Myr" << endl;
    }
    save_as_csv(log_p, v, "CRDiff_energy_loss" + file_appendix + ".csv",
                "log_p", "energy loss rate", "energy loss rate");
    NumType ekin = 1.0; ///Ekin in MeV
    NumType p = sqrt((ekin + MASS_P) * (ekin + MASS_P) - MASS_P * MASS_P) / P0; ///p at Ekin
    cout << " energy loss at Ekin = " << ekin << " MeV: p = " << p << ", -dE/dt(Ekin=" << ekin << "MeV) = "
         << -energy_loss_rate(log(p)) * exp(log(p)) / sqrt(1.0 + (mass_p / p) * (mass_p / p)) * B0
         << " MeV/Myr" << endl;
    cout << " energy loss at p0 = " << P0 << " MeV/c: -dp/dt(p0) = b0 = "
         << -energy_loss_rate(log(P0)) * exp(log(P0)) * B0
         << " MeV/c/Myr" << endl;
}

void
CRDiff::plot_diffusion_coefficient(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &delta,
                                   const std::string &file_appendix) {
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    VecNum kappa(log_p.size());
    for (SizeType i = 0; i < log_p.size(); i++) {
        ///beta = (p/m) / sqrt(1 + (p/m)^2)
        NumType beta = (exp(log_p[i]) / mass_p) /
                       sqrt(1.0 + exp(2.0 * log_p[i]) / (mass_p * mass_p)); ///<beta = v/c
        ///kappa = beta * (1 + p/m)^delta
        kappa[i] = beta * pow(1 + exp(log_p[i]) / mass_p, delta); ///<kappa
//        kappa[i] = exp(delta * log_p[i]); ///<power law kappa
    }

    save_as_csv(log_p, kappa, "CRDiff_diffusion_coefficient" + file_appendix + ".csv",
                "log_p", "$\\kappa / \\kappa_0$", "Diffusion coefficient");
    NumType ekin = 1.0; ///Ekin in MeV
    NumType p = sqrt((ekin + MASS_P) * (ekin + MASS_P) - MASS_P * MASS_P) / P0; ///p at Ekin
    NumType beta = (p / mass_p) / sqrt(1.0 + p * p / (mass_p * mass_p)); ///<beta = v/c
    cout << "Diffusion coefficient at Ekin = " << ekin << " MeV: p = " << p << ", kappa(Ekin=" << ekin << "MeV) = "
         << beta * pow(1 + p / mass_p, delta) * KAPPA0
         << " kpc^2/Myr" << endl;
}


void CRDiff::generate_template_varying_grid(VecNum_I &r, VecNum_I &z, VecNum_I &log_p, VecNum_I &t, VecNum_I &steps,
                                            NumType_I &h, NumType_I &delta, [[gnu::unused]] NumType_I &power_n,
                                            NumType_I &gamma, const std::string &file_appendix) {
    double timer_start, timer_now, timer_last;
    timer_now = timer_start = omp_get_wtime();

    VecNum dr = calculate_dx(r);
    VecNum dz = calculate_dx(z);
    NumType dt, dlog_p;
    dlog_p = log_p[1] - log_p[0];
    SizeType index_z0, end_z; ///<z0-,end-index of disk in z
    VecNum t_out;
    t_out.reserve(steps.size());
    SizeType index_t = 0;

    VecND u;

    ///Open .hdf5 file
    hid_t file_id, dataset_id;
    string file_name = "greens_template" + file_appendix + ".hdf5";
    file_id = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    ///Save log_p and r
    save_in_hdf5(log_p, dataset_id, file_id, "log_p");
    H5Dclose(dataset_id);
    save_in_hdf5(r, dataset_id, file_id, "r");
    H5Dclose(dataset_id);

    ///Prepare dataspace for t
    hsize_t dims_t[1] = {0};
    hsize_t max_dims_t[1] = {H5S_UNLIMITED};
    hsize_t chunk_dims_t[1] = {1000};
    hid_t prop_t = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop_t, 1, chunk_dims_t);
    hid_t dataspace_t = H5Screate_simple(1, dims_t, max_dims_t);
    hid_t dataset_t = H5Dcreate2(file_id, "t", H5T_IEEE_F64BE, dataspace_t, H5P_DEFAULT, prop_t, H5P_DEFAULT);
    H5Pclose(prop_t);
    H5Sclose(dataspace_t);

    hsize_t offset_t[1] = {0};
    hsize_t count_t[1] = {1};
    hid_t memspace_t = H5Screate_simple(1, count_t, nullptr);

    ///Prepare dataspace for u
    hsize_t dims[3] = {0, log_p.size(), r.size()};
    hsize_t max_dims[3] = {H5S_UNLIMITED, log_p.size(), r.size()};
    hsize_t chunk_dims[3] = {1, log_p.size(), r.size()};
    hid_t prop_id = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop_id, 3, chunk_dims);
    hid_t dataspace_id = H5Screate_simple(3, dims, max_dims);
    dataset_id = H5Dcreate2(file_id, "u", H5T_IEEE_F64BE, dataspace_id, H5P_DEFAULT, prop_id, H5P_DEFAULT);
    H5Pclose(prop_id);
    H5Sclose(dataspace_id);

    hsize_t offset_u[3] = {0, 0, 0};
    hsize_t count_u[3] = {1, 1, r.size()};
    hid_t memspace_u = H5Screate_simple(3, count_u, nullptr);

    ///Close file
    H5Dclose(dataset_t);
    H5Dclose(dataset_id);
    H5Fclose(file_id);

    ///Initial condition
#if USE_INITIAL_CONDITION == 1
    calculate_u0(u, gamma, r, z, log_p, 2.0 * t[0]);
#elif USE_INITIAL_CONDITION == 2
    calculate_u0_minh(u, gamma, r, z, log_p, z[1], r[1]);
#elif USE_INITIAL_CONDITION == 3
    NumType r_min = pow(10.0, 1.5) * 1e-3 / LAMBDA; //Initial condition Minh
    NumType z_min = pow(10.0, 1) * 1e-3 / LAMBDA;
    calculate_u0_minh(u, gamma, r, z, log_p, z_min, r_min);
#endif

    ///Start-/End-index of disk in z
    index_z0 = 0;
    if (h >= z.back()) {
        end_z = z.size() - 1;
    } else {
        SizeType i = 0;
        while (z[i + index_z0] < h) { i++; }
        end_z = index_z0 + i;
    }

    timer_now = omp_get_wtime();
    cout << "Initialized calculation of " << file_name << " in " << timer_now - timer_start << " seconds." << endl;
    timer_last = omp_get_wtime();
    timer_start = timer_last;
    SizeType last_step = 0;
    for (SizeType i = 0; i < steps.size(); i++) {
        if (i != 0 and steps[i] == last_step)
            continue;
        t_out.push_back(t[steps[i]]);
        index_t = t_out.size() - 1;

#if USE_CALCULATE_STEPS == 1
        for (SizeType step = last_step + 1; step <= steps[i]; step++) {
            dt = t[step] - t[step - 1];
            diffusion_step_r_z(u, dr, dz, dt, delta, r, log_p);
            calculate_step_p(u, dlog_p, dt, power_n, log_p, end_z);
        }
#elif USE_CALCULATE_STEPS == 2
        for (SizeType step = last_step + 1; step <= steps[i]; step++) { ///Real CR diff
            dt = t[step] - t[step - 1];
            diffusion_step_r_z_2(u, dr, dz, dt, delta, r, log_p);
            calculate_step_p_2(u, dlog_p, dt, log_p, end_z);
        }
#endif

        ///Open file
        file_id = H5Fopen(file_name.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        ///Extend and write t
        dims_t[0] += 1;
        dataset_t = H5Dopen2(file_id, "t", H5P_DEFAULT);
        H5Dset_extent(dataset_t, dims_t);
        dataspace_t = H5Dget_space(dataset_t);
        offset_t[0] = index_t;
        H5Sselect_hyperslab(dataspace_t, H5S_SELECT_SET, offset_t, nullptr, count_t, nullptr);
        H5Dwrite(dataset_t, H5T_NATIVE_DOUBLE, memspace_t, dataspace_t, H5P_DEFAULT, t_out.data() + index_t);
        H5Sclose(dataspace_t);
        ///Extend hdf5 file and write u(z=0)
        dims[0] += 1;
        dataset_id = H5Dopen2(file_id, "u", H5P_DEFAULT);
        H5Dset_extent(dataset_id, dims);
        dataspace_id = H5Dget_space(dataset_id);
        offset_u[0] = index_t;
        IndexType index = {0, index_z0, 0};
        for (SizeType i_logp = 0; i_logp < log_p.size(); i_logp++) {
            index[0] = i_logp;
            offset_u[1] = i_logp;
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset_u, nullptr, count_u, nullptr);
            VecNum u_out = u.subvector(2, index);
            ///Scale back with exp(-log_p)
            NumType log_p_i = log_p[i_logp];
            auto func_scaleback = [&log_p_i](NumType_I &un_i) {
                return exp(-log_p_i) * un_i;
            };
            u_out = vec_apply(u_out, func_scaleback);
            H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, memspace_u, dataspace_id, H5P_DEFAULT, u_out.data());
        }
        H5Sclose(dataspace_id);
        ///Close file
        H5Dclose(dataset_t);
        H5Dclose(dataset_id);
        H5Fclose(file_id);

        timer_now = omp_get_wtime();
        cout << "Calculated steps: " << steps[i] << "/" << steps.back()
             << " (last: " << steps[i] - last_step << ", " << timer_now - timer_last
             << "s, total: " << timer_now - timer_start << "s / "
             << (timer_now - timer_start) / steps[i] * steps.back() << "s) \r" << flush;
        timer_last = timer_now;
        last_step = steps[i];
    }
    H5Sclose(memspace_t);
    H5Sclose(memspace_u);
    timer_now = omp_get_wtime();
    cout << "Finished calculation of " << file_name << " in " << timer_now - timer_start << " seconds.     " << endl;
}