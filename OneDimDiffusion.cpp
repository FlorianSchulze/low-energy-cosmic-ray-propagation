//
// Created by Flo on 04.05.2020.
//

#include "OneDimDiffusion.hpp"
#include "NDNumerics.hpp"
#include "parameters.hpp"
#include <cmath>
#include <sstream>

using namespace std;
using namespace plot;
using namespace NDNumerics;

NumType onedim::psi_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0) {
    return exp(-x * x / (4 * t * kappa0)) / (2 * sqrt(M_PI * t * kappa0));
}

NumType onedim::psi_r_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0) {
    return exp(-x * x / (4 * t * kappa0)) / (4 * M_PI * t * kappa0);
}

void onedim::advection_plot(NumType_I &dx, NumType_I &dt, const int &steps, NumType_I &x_start, NumType_I &x_end,
                            NumType_I &v, NumType_I &epsilon, const std::string &file_appendix) {
    VecNum x = arange(x_start, x_end, dx);

    VecND u0;
    function < NumType(VecNum_I & ) > f0 = [&epsilon](VecNum_I &val) { return dirac_delta(val[0], epsilon); };
    u0.apply({x}, f0);
    u0[0] = 0;
    u0.at(-1) = 0;
    VecND u = u0;
    VecND velocity(u.get_dimension(), u.get_size(), v);

    VecND ut;
    function < NumType(VecNum_I & ) > ft = [&dt, &steps, &v, & epsilon](VecNum_I &val) {
        return dirac_delta(val[0] - v * (dt * steps), epsilon);
    };
    ut.apply({x}, ft);
    save_as_csv(x, ut.subvector(0, {0}), "Advection_Theory" + file_appendix + ".csv", "x", "u(x)", "Theory");
//    save_as_csv(x, u0.subvector(0, {0}), "Advection_u0" + file_appendix + ".csv", "x", "u(x)", "u0");

//    for (int i = 0; i < steps; i++) {
//        lax_method(u, dx, dt, v, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_Lax" + file_appendix + ".csv", "x", "u(x)", "Lax");

//    u = u0;
//    VecND un;
//    VecND um;
//    function<NumType(VecNum_I &)> fm = [&dt, &steps, &v](VecNum_I &val) {
//        return non_diffusive_theory(val[0], -dt, v);
//    };
//    um.apply({x}, fm);
//    for (int i = 0; i < steps; i++) {
//        un = u;
//        staggered_leapfrog(u, um, dx, dt, v, 0);
//        um = un;
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_SL" + file_appendix + ".csv", "x", "u(x)", "staggered leapfrog");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        lax_wendroff(u, dx, dt, v, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_LW" + file_appendix + ".csv", "x", "u(x)", "Lax-Wendroff");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        upwind(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_upwind" + file_appendix + ".csv", "x", "u(x)", "Upwind");

    u = u0;
    for (int i = 0; i < steps; i++) {
        mpdata(u, dx, dt, velocity, 0);
    }
    save_as_csv(x, u.subvector(0, {0}), "Advection_mpdata" + file_appendix + ".csv", "x", "u(x)", "mpdata");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        mpdata_2(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_mpdata_2" + file_appendix + ".csv", "x", "u(x)", "mpdata_2");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        advection_crank_nicolson(u, dx, dt, v, 0, 0.5);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_crank_nicolson" + file_appendix + ".csv", "x", "u(x)", "Crank-Nicolson");

    u = u0;
    for (int i = 0; i < steps; i++) {
        advection_scheme(u, dx, dt, velocity, 0, 0.5);
    }
    save_as_csv(x, u.subvector(0, {0}), "Advection_own" + file_appendix + ".csv", "x", "u(x)", "own");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        semi_lagrangian(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_semi_lagrangian" + file_appendix + ".csv", "x", "u(x)",
//                "Semi Lagrangian");

//    VecNum dx_vec = calculate_dx(x);
//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        semi_lagrangian(u, dx_vec, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_semi_lagrangian_varying" + file_appendix + ".csv", "x", "u(x)",
//                "Semi Lagrangian non-equidistant");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        semi_lagrangian_2(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "Advection_semi_lagrangian_2" + file_appendix + ".csv", "x", "u(x)",
//                "Semi Lagrangian 2");
}


void onedim::varying_grid_plot(VecNum_I &x, VecNum_I &t, NumType_I &kappa0, const std::string &file_appendix,
                               NumType t_end) {
    VecNum dx = calculate_dx(x);
    if (t_end < 0) {
        t_end = t.back();
    } else {
        SizeType step;
        for (step = 0; step < t.size() - 1 and t[step] < t_end; step++) {}
        t_end = t[step];
    }

    VecND u0;
    function < NumType(VecNum_I & ) > f0 = [&t, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], t[0], kappa0);
    };
    u0.apply({x}, f0);
//    u0[0] = 0;
    u0.at(-1) = 0;
    VecND u;

    VecND ut;
    function < NumType(VecNum_I & ) > ft = [&t_end, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], t_end, kappa0);
    };
    ut.apply({x}, ft);
    save_as_csv(x, ut.subvector(0, {0}), "OneDimVarDiff_theory" + file_appendix + ".csv",
                "x", "u(x)", "Theory");

    u = u0;
    for (SizeType i = 1; i < t.size() and t[i] <= t_end; i++) {
//        diffusion_ftcs(u, dx, t[i] - t[i-1], kappa0, 0);
//        diffusion_implicit(u, dx, t[i] - t[i-1], kappa0, 0);
        diffusion_crank_nicolson(u, dx, t[i] - t[i - 1], kappa0, 0, 1);
    }
    save_as_csv(x, u.subvector(0, {0}), "OneDimVarDiff_numerical" + file_appendix + ".csv",
                "x", "u(x)", "numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function < NumType(NumType_I & , NumType_I & ) > func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimVarDiff_abs" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimVarDiff_rel" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(x, diff.subvector(0, {0}), "OneDimVarDiff_absrel" + file_appendix + "_error.csv",
                "x", "$\\Delta$ u(x)", "absolute difference / max_value");
}

void onedim::varying_grid_plot_r(VecNum_I &x, VecNum_I &t, NumType_I &kappa0, const std::string &file_appendix,
                                 NumType t_end) {
    VecNum dx = calculate_dx(x);
    if (t_end < 0) {
        t_end = t.back();
    } else {
        SizeType step;
        for (step = 0; step < t.size() - 1 and t[step] < t_end; step++) {}
        t_end = t[step];
    }

    VecND u0;
    function < NumType(VecNum_I & ) > f0 = [&t, &kappa0](VecNum_I &val) {
        return psi_r_theory(val[0], t[0], kappa0);
    };
    u0.apply({x}, f0);
    u0.at(-1) = 0;
    VecND u;

    /// kappa / r for the first order term.
    auto kappa_r_func = [&x, &kappa0](VecND &v_in, SizeType_I &subvector_index, IndexType_I &index) {
        for (SizeType j = 1; j < v_in.get_size(subvector_index); j++) {
            v_in.at(j, subvector_index, index) = -kappa0 / x[j];
        }
        v_in.at(0, subvector_index, index) = v_in.at(1, subvector_index, index);
    };
    VecND kappa_r(u0.get_dimension(), u0.get_size());
    kappa_r.apply_subvector(kappa_r_func, 0);

    VecND ut;
    function < NumType(VecNum_I & ) > ft = [&t_end, &kappa0](VecNum_I &val) {
        return psi_r_theory(val[0], t_end, kappa0);
    };
    ut.apply({x}, ft);
    save_as_csv(x, ut.subvector(0, {0}), "OneDimVarDiffR_theory" + file_appendix + ".csv",
                "x", "u(x)", "Theory");

    u = u0;
    for (SizeType i = 1; i < t.size() and t[i] <= t_end; i++) {
        diffusion_crank_nicolson(u, dx, t[i] - t[i - 1], kappa0, 0, 1);
        semi_lagrangian(u, dx, t[i] - t[i - 1], kappa_r, 0);
    }
    save_as_csv(x, u.subvector(0, {0}), "OneDimVarDiffR_numerical" + file_appendix + ".csv",
                "x", "u(x)", "numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function < NumType(NumType_I & , NumType_I & ) > func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimVarDiffR_abs" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimVarDiffR_rel" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(x, diff.subvector(0, {0}), "OneDimVarDiffR_absrel" + file_appendix + "_error.csv",
                "x", "$\\Delta$ u(x)", "absolute difference / max_value");
}

VecNum onedim::varying_grid_error(VecNum_I &steps, VecNum_I &x, VecNum_I &t, NumType_I &kappa0,
                                  const std::string &file_appendix) {
    VecNum dx = calculate_dx(x);

    VecND u;
    function < NumType(VecNum_I & ) > f0 = [&t, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], t[0], kappa0);
    };
    u.apply({x}, f0);
//    u[0] = 0;
    u.at(-1) = 0;

    VecNum ret_diff;

    int step, last_step = 0;
    for (auto &step_num: steps) {
        step = (int) step_num;
        VecND ut;
        function < NumType(VecNum_I & ) > ft = [&t, &step, &kappa0](VecNum_I &val) {
            return psi_theory(val[0], t[step], kappa0);
        };
        ut.apply({x}, ft);

        for (int i = last_step + 1; i <= step; i++) {
            diffusion_crank_nicolson(u, dx, t[i] - t[i - 1], kappa0, 0, 1);
        }

//        ret_diff.push_back(vec_max(difference(ut.get_values(), u.get_values()))); ///Absolute difference
//        ret_diff.push_back(vec_max(relative_difference(ut.get_values(), u.get_values()))); ///relative difference
        ret_diff.push_back(max_difference_relative(ut.get_values(), u.get_values())); ///max difference relative

        last_step = step;
    }
    save_as_csv(steps, ret_diff, "OneDimVarDiffError_steps" + file_appendix + ".csv",
                "steps", "$\\Delta u$", file_appendix);
    VecNum t_plot(steps.size());
    for (SizeType i = 0; i < steps.size(); i++) {
        t_plot[i] = t[(int) steps[i]];
    }
    save_as_csv(t_plot, ret_diff, "OneDimVarDiffError_t" + file_appendix + ".csv",
                "t", "$\\Delta u$", file_appendix);
    return ret_diff;
}

VecNum onedim::varying_grid_error_r(VecNum_I &steps, VecNum_I &x, VecNum_I &t, NumType_I &kappa0,
                                    const std::string &file_appendix) {
    VecNum dx = calculate_dx(x);

    VecND u;
    function < NumType(VecNum_I & ) > f0 = [&t, &kappa0](VecNum_I &val) {
        return psi_r_theory(val[0], t[0], kappa0);
    };
    u.apply({x}, f0);
    u.at(-1) = 0;

    /// kappa / r for the first order term.
    auto kappa_r_func = [&x, &kappa0](VecND &v_in, SizeType_I &subvector_index, IndexType_I &index) {
        for (SizeType j = 1; j < v_in.get_size(subvector_index); j++) {
            v_in.at(j, subvector_index, index) = -kappa0 / x[j];
        }
        v_in.at(0, subvector_index, index) = v_in.at(1, subvector_index, index);
    };
    VecND kappa_r(u.get_dimension(), u.get_size());
    kappa_r.apply_subvector(kappa_r_func, 0);

    VecNum ret_diff;

    int step, last_step = 0;
    for (auto &step_num: steps) {
        step = (int) step_num;
        VecND ut;
        function < NumType(VecNum_I & ) > ft = [&t, &step, &kappa0](VecNum_I &val) {
            return psi_r_theory(val[0], t[step], kappa0);
        };
        ut.apply({x}, ft);

        for (int i = last_step + 1; i <= step; i++) {
            diffusion_crank_nicolson(u, dx, t[i] - t[i - 1], kappa0, 0, 1);
            semi_lagrangian(u, dx, t[i] - t[i - 1], kappa_r, 0);
        }

//        ret_diff.push_back(vec_max(difference(ut.get_values(), u.get_values()))); ///Absolute difference
//        ret_diff.push_back(vec_max(relative_difference(ut.get_values(), u.get_values()))); ///relative difference
        ret_diff.push_back(max_difference_relative(ut.get_values(), u.get_values())); ///max difference relative

        last_step = step;
    }
    save_as_csv(steps, ret_diff, "OneDimVarDiffRError_steps" + file_appendix + ".csv",
                "steps", "$\\Delta u$", file_appendix);
    VecNum t_plot(steps.size());
    for (SizeType i = 0; i < steps.size(); i++) {
        t_plot[i] = t[(int) steps[i]];
    }
    save_as_csv(t_plot, ret_diff, "OneDimVarDiffRError_t" + file_appendix + ".csv",
                "t", "$\\Delta u$", file_appendix);
    return ret_diff;
}

