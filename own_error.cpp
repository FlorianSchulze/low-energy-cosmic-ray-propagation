//
// Created by flosc on 13.05.2020.
//

#include "own_error.hpp"
#include <iostream>

void own_catch(const own_error &err) {
    printf("ERROR: %s\n in file %s at line %d\n", err.message, err.file, err.line);
    exit(1);
}