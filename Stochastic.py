"""
Calculate source distribution and stochastic part
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, integrate
import glob
import h5py
import time
import multiprocessing
from Templates import *


def plot_cr_flux_data(new_figure=True):
    """Plot the CR flux data of Voyager and AMS. Use current figure if new_figure is False."""
    file_list = glob.glob('plots/data/Protons_*_Ekin.csv')
    if not file_list:
        return -1
    if new_figure:
        plt.figure()

    for file in file_list:
        ekin, ekin_lo, ekin_up, y, yerr_lo, yerr_up = np.loadtxt(file, unpack=True, skiprows=3,
                                                                 usecols=(0, 1, 2, 3, 8, 9), delimiter=',')
        label = file.split('_')[1]
        if 'Voyager' in file:
            label = 'Voyager 1'
        if 'AMS' in file:
            label = 'AMS-2'
            max_element = 30
            ekin = ekin[:max_element] * 1e3
            ekin_lo = ekin_lo[:max_element] * 1e3
            ekin_up = ekin_up[:max_element] * 1e3
            y = y[:max_element] * 1e-3
            yerr_lo = yerr_lo[:max_element] * 1e-3
            yerr_up = yerr_up[:max_element] * 1e-3
        plt.errorbar(x=ekin, y=y, xerr=(ekin - ekin_lo, ekin_up - ekin), yerr=(yerr_lo, yerr_up), label=label, fmt='.')

    if new_figure:
        plt.legend()
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('Experimental CR flux')
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('ExpCRFlux.pdf')

    return 0


def plot_ionisation_data(new_figure=True, do_label=True, do_plot_phan=True):
    """Plot the ionisation rate data, and the inisation rate by Phan et al. if do_plot_phan True.
    Use current figure if new_figure is False."""
    file_list = glob.glob('plots/data/ionisation/*.csv')
    if not file_list:
        return -1
    if new_figure:
        plt.figure()

    for file in file_list:
        x, x_lo, x_up, y, yerr = np.loadtxt(file, unpack=True, skiprows=1, delimiter=',')
        if 'Indriolo' in file:
            """x and x_lo are swapped"""
            dummy = x
            x = x_lo
            x_lo = dummy
            """upper errors doens't fit"""
            x_up = x + x - x_lo
        if not isinstance(x, np.ndarray):
            x = np.array([x])
            x_lo = [x_lo]
            x_up = [x_up]
            y = [y]
            yerr = [yerr]
        label = file.split('/')[-1].split('.')[0]
        if not do_label:
            label = None
        fmt = 'xr'
        if 'Caselli' in file:
            fmt = '.b'
        if 'Williams' in file:
            fmt = '^b'
        if 'Maret' in file:
            fmt = '*m'
        if 'Indriolo' in file:
            if 'data_points' in file:
                fmt = 'sk'
            if 'upper_limits' in file:
                fmt = 'vy'
        plt.errorbar(x=x, y=y, xerr=(x - x_lo, x_up - x), yerr=yerr, label=label, fmt=fmt)

    if do_plot_phan:
        val = 1.23196e-17
        label = 'Phan et al.'
        plt.axhline(val, linestyle='--', color='r', label=label)

    if new_figure:
        plt.xlabel(r'$N_{H_2}$ [$cm^{-2}$]')
        plt.ylabel(r'$\zeta (N_{H_2})$ [$s^{-1}$]')
        plt.title('Experimental ionisation rate')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('ExpIonisationRate.pdf')
    return 0


def source_distribution(source_count=None, t_start=1e-3, t_end=1e2, r_max=R, use_rng=False):
    """
    Calculates the source distribution

    :param source_count: count of SN sources, if None it is calculated put of t_end with 3SN/100yr
    :param t_start: start time in Myr
    :param t_end: end time in Myr
    :param r_max: maximal distance in kpc
    :param use_rng: True: the default_rgn is used; False: np.random.uniform/power is used
    :return: t0, r0 as numpy array
    """
    if source_count is None:
        source_count = int(3 * t_end / 1e-4)
    if use_rng:
        """Better random generator for parallel computing"""  # Problems on the Cluster
        # rg = np.random.Generator(np.random.PCG64())
        rg = np.random.default_rng()
        """time distribution of sources, homogeneous distribution in t"""
        t0 = rg.uniform(t_start, t_end, source_count)
        """spacial distribution of sources, homogeneous distribution in r"""
        r0 = r_max * rg.power(2, source_count)
    else:
        t0 = np.random.uniform(t_start, t_end, source_count)
        r0 = r_max * np.random.power(2, source_count)
    return t0, r0


def preprocess_u_for_interpolation(u_in, t_in, r_in):
    """
    Preprocess u, t, r so they can be used by interpolate_template

    :param u_in: u like it is saved in the template
    :param t_in: t like it is saved in the template
    :param r_in: r like it is saved in the template
    :return: log_u, log_t, log_r as input for interpolate template
    """
    log_t = np.log(t_in)
    r_in[0] = np.finfo(np.float64).eps  # use minimal value instead of 0, so log doesn't get problems
    log_r = np.log(r_in)
    u_in = np.swapaxes(u_in, 1, 2)  # u[t, r, log_p]
    # u_in = u_in * q0 / (lamb ** 3)
    # u_in[u_in == 0] = np.finfo(np.float64).eps  # use minimal value instead of 0, so log doesn't get problems
    log_u = np.log(u_in)
    log_u[log_u == -np.inf] = np.finfo(np.float64).min  # use minimal value instead of -inf
    return log_u, log_t, log_r


def interpolate_template(log_u, log_t, log_r, t0, r0):
    """
    Interpolate the template log_u in r and t.

    :param log_u: log(u) (u is the template) but with swapped axis, so u is in the form u[t, log_r, log_p]
    :param log_t: logarithmic t axis, log(t)
    :param log_r: logarithmic r axis, log(r)
    :param t0: array of times t_0 of the sources in Myr
    :param r0: array of position r_0 of the sources in kpc
    :return: interpolated spectrum for every source u[i, p]
    """
    log_t0 = np.log(t0 / tau)
    log_r0 = np.log(r0 / lamb)

    log_t0[log_t0 < log_t[0]] = log_t[0]  # If the t0 is smaller then the minimal t, the minimal t is chosen
    log_t0_interior = log_t0[log_t0 <= log_t[-1]]
    log_r0_interior = log_r0[log_t0 <= log_t[-1]]
    log_t0_exterior = log_t0[log_t0 > log_t[-1]]
    log_r0_exterior = log_r0[log_t0 > log_t[-1]]

    u_out = np.empty((0, log_u.shape[2]), dtype=np.float64)
    """Interpolate inside the gird"""
    if log_t0_interior.size > 0:
        points = np.transpose([log_t0_interior, log_r0_interior])
        val = interpolate.interpn((log_t, log_r), log_u, points, method='linear')
        u_out = np.concatenate((u_out, val))

    """Interpolate outside the grid"""
    if log_t0_exterior.size > 0:
        points = np.transpose([np.full(log_t0_exterior.shape, log_t[-1]), log_r0_exterior])
        val = interpolate.interpn((log_t, log_r), log_u, points, method='linear')
        points_m = np.transpose([np.full(log_t0_exterior.shape, log_t[-2]), log_r0_exterior])
        val_m = interpolate.interpn((log_t, log_r), log_u, points_m, method='linear')

        log_t0_exterior_b = np.broadcast_to(np.reshape(log_t0_exterior, (log_t0_exterior.size, 1)), val.shape)
        a = (val - val_m) / (log_t[-1] - log_t[-2])
        val = val + a * (log_t0_exterior_b - log_t[-1])

        u_out = np.concatenate((u_out, val))

    u_out = np.exp(u_out) * q0 / (lamb ** 3)
    return u_out


def plot_interpolate_time_development(file_appendix='', r0_array=[0.1], p0_array=[1, 100, 10000]):
    """
    Plot the time development of the CR density calculated with the interpolation.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param r0_array: The distances r_0 to be plotted (array)
    :param p0_array: The impulse p to be plotted (array)
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    log_u, log_t, log_r = preprocess_u_for_interpolation(u, t, r)
    t = t * tau
    u = u * q0 / (lamb ** 3)
    t_interpolate = np.logspace(-3, 3, 500)

    r0_array = np.array(r0_array, ndmin=1)
    u_interpolate = np.zeros((len(t_interpolate), len(r0_array), len(log_p)))
    for i, dt in enumerate(t_interpolate):
        dt_array = np.array([dt] * r0_array.size, ndmin=1)
        u_interpolate[i] = interpolate_template(log_u, log_t, log_r, dt_array, r0_array)

    plt.figure()
    for i, r0 in enumerate(r0_array):
        index_r0 = np.abs(r - r0 / lamb).argmin()
        for p_val in p0_array:
            index_p0 = np.abs(log_p - np.log(p_val / p0)).argmin()
            p = p0 * np.exp(log_p[index_p0])
            y = (p / p0) ** gamma * t ** (3 / 2) * u[:, index_p0, index_r0]
            label = 'r=%.1fkpc, p=%.1eMeV/c' % (r[index_r0] * lamb, p)
            plt.plot(t, y, '.', label=label)
    plt.gca().set_prop_cycle(None)
    label_interpolate = 'interpolated'
    for i, r0 in enumerate(r0_array):
        for p_val in p0_array:
            index_p0 = np.abs(log_p - np.log(p_val / p0)).argmin()
            p = p0 * np.exp(log_p[index_p0])
            y_interpolate = (p / p0) ** gamma * t_interpolate ** (3 / 2) * u_interpolate[:, i, index_p0]
            plt.plot(t_interpolate, y_interpolate, '--', label=label_interpolate)
            label_interpolate = None

    plt.xlabel('$t-t_0$ [Myr]')
    plt.ylabel(r'$(\frac{p}{p_0})^{\Gamma} (\frac{t-t_0}{Myr})^{3/2} \Psi(r,p,t)$ [$kpc^{-3} (MeV/c)^{-1}$]')
    # plt.title('Interpolated time development')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(bottom=1e40, top=1e50)
    plt.grid()
    plt.tight_layout()
    plt.savefig('CRDiffTimeDevelopmentInterpolated%s.pdf' % file_appendix)
    return 0


def calculate_scaling_factor(ekin, j0):
    """Calculate the scaling factor by comparing the calculated j0 with the AMS data"""
    ekin_ams, y = np.loadtxt('plots/data/Protons_AMS02_Ekin.csv', unpack=True, skiprows=3, usecols=(0, 3),
                             delimiter=',')
    scaling_index = -1
    ekin_ams *= 1e3
    scaling_index_ams = np.argmin(abs(ekin_ams - ekin[scaling_index]))
    j0_ams = y[scaling_index_ams] * 1e-3
    scaling_factor = j0_ams / j0[scaling_index]
    return scaling_factor


def spectrum_mean(file_appendix='', t_end=1e2, rescale=False, do_plot=True):
    """
    Calculate the mean of the CR spectrum with a uniform distribution.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t_end: end time in Myr
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param do_plot: if True the spectrum is plotted
    :return -1 if file don't exit, j0_mean else
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1

    file = h5py.File(file_list[0], 'r')
    t = np.array(file['t'][...], dtype=np.float64)
    log_p = np.array(file['log_p'][...], dtype=np.float64)
    r = np.array(file['r'][...], dtype=np.float64)
    u = np.array(file['u'][...], dtype=np.float64)
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)
    ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

    """Calculating man via summation"""
    dr = r[1:] - r[:-1]
    dr = np.append(dr, [0])
    dr[r > R] = 0  # Only sum to R
    dt = t[1:] - t[:-1]
    dt = np.append(dt, [0])
    dt[t > t_end] = 0  # Only sum to t_max
    u = np.tensordot(u, r * dr, axes=(2, 0))  # Sum in r
    u = np.tensordot(u, dt, axes=(0, 0))  # Sum in t
    source_count = int(3 * t_end / 1e-4)
    source_count = int(3 * t_end / 1e-4 * R ** 2 / 15 ** 2)  # Same as Minh
    u = u * 2. / (R ** 2 * t_end) * source_count  # Normalization

    beta = (p / mass_p) / np.sqrt(1. + (p / mass_p) ** 2)
    """Calculate j0: j0 = beta / (4 pi) * sqrt(1 + (m c / p)^2) u"""
    j0 = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * u * 1.02e-50  # intenesity in (m^2 s sr MeV)^-1
    j0_phan = intensity_phan(ekin)  # intensity by phan et al

    if rescale:
        scaling_factor = calculate_scaling_factor(ekin, j0)
        j0 *= scaling_factor
        u *= scaling_factor

    np.savetxt('MeanIntegrated%s.csv' % file_appendix, np.transpose([ekin, j0]),
               header='Ekin [MeV], mean (integrated) [(m^2 s sr MeV)^-1)]')

    if do_plot:
        fig = plt.figure()
        plt.plot(ekin, j0, label='Mean Spectrum')
        plt.plot(ekin, j0_phan, label='Fitted')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffMeanSpectrum%s.pdf' % file_appendix)

    return j0


def added_spectrum(file_appendix='', t_end=1e2, rescale=False, load_hdf5=False, do_plot=True,
                   do_plot_hist=False, do_plot_strongest=False):
    """
    Calculate and plot the CR spectrum created by a distribution of sources sources.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t_end: end time in Myr
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param load_hdf5: if True the file 'one_spectrum%s.hdf5' is load instead of calculation the distribution
    :param do_plot: if True the plot 'CRDiffAddedSpectrum' is created
    :param do_plot_hist: if True the histogram of source spectrum and cumulative spectrum plot is created
    :param do_plot_strongest: if True the plot with only the strongest sources is created
    :return: ekin, u (kinetic energy, added spectrum), or -1 if file don't exist
    """
    file_list = glob.glob('one_spectrum%s.hdf5' % file_appendix)
    if load_hdf5 and not file_list:
        return -1
    if load_hdf5:
        file = h5py.File(file_list[0], 'r')
        ekin = file['ekin'][...]
        p = file['p'][...]
        u_interpol = file['u'][...]
        t0 = file['t0'][...]
        r0 = file['r0'][...]
    else:
        file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
        if not file_list:
            return -1

        print('Start calculating source distribution.')
        timer_start = time.time()
        t_start = 1e-3
        r_max = R
        t0, r0 = source_distribution(t_start=t_start, t_end=t_end, r_max=r_max)
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        """Sort t0, so the indexes in u wont change because of splitting t0"""
        index_sorted = np.argsort(t0)
        t0 = t0[index_sorted]
        r0 = r0[index_sorted]

        file = h5py.File(file_list[0], 'r')
        t_in = np.array(file['t'][...], dtype=np.float64)
        log_p = np.array(file['log_p'][...], dtype=np.float64)
        r_in = np.array(file['r'][...], dtype=np.float64)
        u_in = np.array(file['u'][...], dtype=np.float64)
        log_p = log_p[1:]  # cut of first point
        u_in = u_in[:, 1:, :]

        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

        print('Start calculating preprocessing u.')
        timer_start = time.time()
        log_u, log_t, log_r = preprocess_u_for_interpolation(u_in, t_in, r_in)
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        print('Start calculating added spectrum.')
        timer_start = time.time()
        u_interpol = interpolate_template(log_u, log_t, log_r, t0, r0)
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        """Save the results as .hdf5 file"""
        file = h5py.File('one_spectrum%s.hdf5' % file_appendix, 'w')
        file.create_dataset('ekin', data=ekin)
        file.create_dataset('p', data=p)
        file.create_dataset('u', data=u_interpol)
        file.create_dataset('t0', data=t0)
        file.create_dataset('r0', data=r0)

    u = np.sum(u_interpol, 0)
    beta = (p / mass_p) / np.sqrt(1. + (p / mass_p) ** 2)
    """Calculate j0: j0 = beta / (4 pi) * sqrt(1 + (m c / p)^2) u"""
    j0 = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * u * 1.02e-50  # intenesity in (m^2 s sr MeV)^-1
    j0_phan = intensity_phan(ekin)  # intensity by phan et al

    scaling_factor = 1
    if rescale:
        scaling_factor = calculate_scaling_factor(ekin, j0)
        j0 *= scaling_factor
        u *= scaling_factor

    if do_plot:
        fig = plt.figure()
        plt.plot(ekin, j0, label='Added Spectrum')
        plt.plot(ekin, j0_phan, label='Fitted')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffAddedSpectrum%s.pdf' % file_appendix)

    if do_plot_hist:
        fig_cum = plt.figure(1, clear=True)
        fig_hist = plt.figure(2, clear=True)
        percent_values = [0.1, 0.5]
        for val_ekin in [1e0, 1e2, 1e4]:
            index_ekin = np.argmin(abs(ekin - val_ekin))
            label = '$E_{kin}$=%.eMeV' % ekin[index_ekin]
            x = u_interpol[:, index_ekin]
            x = np.sort(x)[::-1]
            x_cum = np.cumsum(x)
            x_cum = x_cum / x_cum[-1]
            number = np.linspace(1, x.size, x.size)

            plt.figure(1)
            label_str = label
            for percent in percent_values:
                n = np.argmin(abs(x_cum - percent)) + 1
                label_str += ', %.f%%: %4i' % (percent * 100, n)
            plt.plot(number, x_cum, label=label_str)

            plt.figure(2)
            x = x[x > 0]
            bins = np.logspace(np.log10(np.min(x)), np.log10(np.max(x)), 100)
            plt.hist(x, bins=bins, label=label)
            """Plot distribution for comparision"""
            # mean = np.mean(x)
            # sigma = np.std(x)
            # dist = np.random.normalsize=x.size)
            # dist = np.random.lognormal(size=x.size)
            # dist = dist[dist>0]
            # bins = np.logspace(np.log10(np.min(dist)), np.log10(np.max(dist)), 100)
            # plt.hist(dist, bins=bins)
        plt.figure(1)
        for percent in percent_values:
            plt.axhline(percent, linestyle='--')
        plt.xlabel('source count')
        plt.ylabel(r'$\frac{\psi_{cumulative}}{\psi_{complete}}$')
        # plt.title('CR Diffusion distribution of the Spectrum - cumulative')
        plt.legend(loc='lower right')
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumDistribution%s_cumulative.pdf' % file_appendix)

        plt.figure(2)
        plt.xlabel('$\\psi$ [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.ylabel('count')
        # plt.title('CR Diffusion distribution of the Spectrum - histogram')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumDistribution%s_hist.pdf' % file_appendix)

    if do_plot_strongest:
        fig = plt.figure()
        plt.plot(ekin, j0, label='Complete Spectrum')
        plt.plot(ekin, j0_phan, label='Fitted')
        x = np.array(u_interpol)
        index_ekin = np.argmin(abs(ekin - 30))
        index_sorted = np.argsort(x[:, index_ekin])[::-1]
        x = x[index_sorted]
        # plt.gca().set_prop_cycle(None)
        for count in [1, 5, 100]:
            x_cum = np.sum(x[:count], axis=0)
            x_cum = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * x_cum * 1.02e-50 * scaling_factor
            obj = plt.plot(ekin, x_cum, ':', label='strongest %i' % count)
            x_cum = np.sum(x[count:], axis=0)
            x_cum = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * x_cum * 1.02e-50 * scaling_factor
            plt.plot(ekin, x_cum, '--', color=obj[-1].get_color())
        plt.plot(0, 0, '--', color='grey', label='without strongest')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum strongest sources')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumStrongest%s.pdf' % file_appendix)

        """Source position"""
        plt.figure()
        i_max = 1000
        # i_max = 20
        for i in range(i_max):
            # obj = plt.plot(r0[index_sorted[i]], t0[index_sorted[i]], ',', color='black', visible=False)
            obj = plt.plot(r0[index_sorted[i]], t0[index_sorted[i]], ',', visible=False)
            fontsize = 80 * x[i, index_ekin] / x[0, index_ekin]
            # fontsize = 12 - np.log(x[i, index_ekin] / x[0, index_ekin])
            plt.text(r0[index_sorted[i]], t0[index_sorted[i]], str(i + 1), fontsize=fontsize,
                     horizontalalignment='center', verticalalignment='center', color=obj[-1].get_color())
        plt.xlabel('$r_0$ [kpc]')
        plt.ylabel('$t_0$ [Myr]')
        # plt.title('CR Diffusion added spectrum strongest sources')
        # plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumStrongestPositions%s.pdf' % file_appendix)
    return ekin, u


def ionrate_energy_loss(ekin, density_n):
    """Energy loss in eV/s"""
    beta = np.sqrt(1. - 1. / (1. + ekin / mass_p))
    return f_ion * density_n * (1.0 + 0.0185 * np.log(beta)) * 2.0 * beta ** 2 / (beta_0 ** 3 + 2.0 * beta ** 3)


def ionrate_CR_density(ekin, interpolator_log_u):
    """CR density N in (cm^3 MeV)^-1"""
    u_val = np.exp(interpolator_log_u(np.log(ekin)))  # Interpolate function
    return (ekin + mass_p) / np.sqrt(ekin ** 2 + 2. * ekin * mass_p) * u_val * 3.4e-65


def ionrate_CR_density_phan(ekin):
    """CR density N with j0 by Phan et al. in (cm^3 MeV)^-1"""
    beta = np.sqrt(1. - 1. / (1. + ekin / mass_p) ** 2)
    return 4. * np.pi / (beta * light_speed_si) * intensity_phan(ekin) * 1e-6


def calculate_ionisation_rate(u, log_ekin, density_n, nH2_array, use_phan=False):
    """
    Calculate the ionisation rate.

    :param u: spectrum u, u[log_ekin]
    :param log_ekin: logarithmic kinetic energy
    :param density_n: the density of the gas, has no effect since it cancels out
    :param nH2_array: the column density nH2, only for the size of array since there is no denpendence on it
    :param use_phan: if True the fitted spectrum by Phan et al. is used instead of u
    :return: ionisation_rate (numpy array with same size as nH2_array)
    """
    interpolator_log_u = interpolate.interp1d(log_ekin, np.log(u), kind='linear', assume_sorted=True)

    def integrand(ekin):
        """abs(dE_kin/dt) * N / 32eV"""
        return ionrate_energy_loss(ekin, density_n) * ionrate_CR_density(ekin, interpolator_log_u) / 32

    if use_phan:
        def integrand(ekin):
            """abs(dE_kin/dt) * N / 32eV"""
            return ionrate_energy_loss(ekin, density_n) * ionrate_CR_density_phan(ekin) / 32

    """integrate with scipy"""
    """primary ionisation rate in cm^-3 s-1"""
    out = integrate.quad(integrand, 1, 10e3)  # 1 MeV - 10 GeV
    primary_ionisation = out[0]

    ionisation_rate = np.zeros(nH2_array.shape)
    for i, x in enumerate(nH2_array):
        ionisation_rate[i] = nu1 * nu2 * nu3 * primary_ionisation / density_n

    return ionisation_rate


def plot_ionisation_rate(file_appendix='', t_end=1e2, rescale=False, load_hdf5=False, do_plot=True,
                         do_plot_added_spectrum=False, do_plot_hist=False, do_plot_strongest=False):
    """
    Plot the ionisation rate.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t_end: end time in Myr
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param load_hdf5: if True the file 'one_spectrum%s.hdf5' is load instead of calculation the distribution
    :param do_plot: if True the plot 'CRDiffIonisationRate' is created
    :param do_plot_added_spectrum: if True the plot 'CRDiffAddedSpectrum' is created
    :param do_plot_hist: if True the histogram of source spectrum and cumulative spectrum plot is created
    :param do_plot_strongest: if True the plot with only the strongest sources is created
    :return: 0, or -1 if file don't exist
    """
    if load_hdf5:
        file_list = glob.glob('one_spectrum%s.hdf5' % file_appendix)
        if not file_list:
            return -1
    else:
        file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
        if not file_list:
            return -1

    ekin, u = added_spectrum(file_appendix, t_end, rescale, load_hdf5, do_plot_added_spectrum,
                             do_plot_hist, do_plot_strongest)
    log_ekin = np.log(ekin)  # log(E_kin / MeV)

    nH2_array = np.power(10, np.linspace(20, 23, 10))
    print('Start calculating ionisation rate.')
    timer_start = time.time()
    ionisation_rate = calculate_ionisation_rate(u, log_ekin, n_ion, nH2_array)
    ionisation_rate_phan = calculate_ionisation_rate(u, log_ekin, n_ion, nH2_array, use_phan=True)
    timer_end = time.time()
    print('Process finished in %f seconds.' % (timer_end - timer_start))

    if do_plot:
        plt.figure()
        plt.plot(nH2_array, ionisation_rate, label='Calculated')
        plt.plot(nH2_array, ionisation_rate_phan, label='Fitted')
        plt.xlabel(r'$N_{H_2}$ [$cm^{-2}$]')
        plt.ylabel(r'$\xi (N_{H_2})$ [$s^{-1}$]')
        # plt.title('Ionisation rate')
        plot_ionisation_data(False, False, True)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffIonisationRate%s.pdf' % file_appendix)
    return 0


shared_finished_processes = multiprocessing.Value('I', 0)  # Variable for counting calculated samples


def calculate_sample(i, u_in, t_in, r_in, source_count, t_start, t_end, r_max, sample_size, timer_start):
    """Calculates one sample and return the u. Used for multiprocessing."""
    t0, r0 = source_distribution(source_count, t_start, t_end, r_max, use_rng=True)
    u_interpol = interpolate_template(u_in, t_in, r_in, t0, r0)
    u = np.sum(u_interpol, 0)
    time_now = time.perf_counter()
    with shared_finished_processes.get_lock():
        shared_finished_processes.value += 1
        i = shared_finished_processes.value
    print(f'Calculated {i}/{sample_size} samples in ' +
          f'{time_now - timer_start :.2f}s / {(time_now - timer_start) * sample_size / i :.2f}s.',
          flush=True, end='\r')
    return u


def generate_sample(template_file_appendix='', file_appendix='', t_end=1e2, sample_size=100, use_parallel=False):
    """
    Generate the samples of different source distributions and save as hdf5 file.

    :param template_file_appendix: file identifier for the green's template,
     the file 'greens_template'+template_file_appendix'.hdf5' is used if it exists.
    :param file_appendix: file identifier for the save-file: 'sample_spectrum'+file_appendix+'.hdf5'
    :param t_end: end time in Myr
    :param sample_size: the size of the sample
    :param use_parallel: use parallelization
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % template_file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t_in = np.array(file['t'][...], dtype=np.float64)
    log_p = np.array(file['log_p'][...], dtype=np.float64)
    r_in = np.array(file['r'][...], dtype=np.float64)
    u_in = np.array(file['u'][...], dtype=np.float64)
    log_p = log_p[1:]  # cut of first point
    u_in = u_in[:, 1:, :]

    p = np.exp(log_p) * p0
    ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

    u_in, t_in, r_in = preprocess_u_for_interpolation(u_in, t_in, r_in)

    source_count = int(3 * t_end / 1e-4)
    t_start = 1e-3
    r_max = R
    source_count = int(3 * t_end / 1e-4 * r_max ** 2 / 15 ** 2)  # Same as Minh
    print('N_s = %.2e' % source_count, source_count)

    u = np.empty((sample_size, u_in.shape[2]), dtype=np.float64)
    print('Start calculating sample_spectrum%s.hdf5 (%i samples).' % (file_appendix, sample_size), flush=True)
    timer_start = time.perf_counter()

    if use_parallel:
        """Use multiprocessing"""  # Problems on the Cluster
        with shared_finished_processes.get_lock():
            shared_finished_processes.value = 0
        with multiprocessing.Pool() as executor:
            iterable = [(i, u_in, t_in, r_in, source_count, t_start, t_end, r_max, sample_size, timer_start)
                        for i in range(sample_size)]
            u = executor.starmap(calculate_sample, iterable)
    else:
        """Use only one process"""
        for i in range(sample_size):
            t0, r0 = source_distribution(source_count, t_start, t_end, r_max)
            u_interpol = interpolate_template(u_in, t_in, r_in, t0, r0)
            u[i] = np.sum(u_interpol, 0)
            time_now = time.perf_counter()
            print('Calculated %i/%i samples in %.2fs / %.2fs.' % (
                i + 1, sample_size, time_now - timer_start, (time_now - timer_start) * sample_size / (i + 1)),
                  flush=True, end='\r')

    """Save the results as .hdf5 file"""
    print('Save file sample_spectrum%s.hdf5...' % file_appendix, flush=True, end='\r')
    file = h5py.File('sample_spectrum%s.hdf5' % file_appendix, 'w')
    file.create_dataset('ekin', data=ekin)
    file.create_dataset('u', data=u)
    timer_end = time.perf_counter()
    print('Finished calculating sample_spectrum%s.hdf5 in %f seconds.     ' % (file_appendix, timer_end - timer_start),
          flush=True)
    return 0


def generate_samples(list_template_file_appendix=[''], list_file_appendix=[''], t_end=1e2, sample_size=100):
    """
    Generate the samples of different source distributions with different greens templates and save as hdf5 file.

    :param list_template_file_appendix: file identifier list for the green's templates,
     the file 'greens_template'+template_file_appendix'.hdf5' is used if it exists.
    :param list_file_appendix: file identifier list for the save-files: 'sample_spectrum'+file_appendix+'.hdf5'
    :param t_end: end time in Myr
    :param sample_size: the size of the sample
    :return: 0, or -1 if one file don't exist
    """
    if len(list_template_file_appendix) != len(list_file_appendix):
        print('list_template_file_appendix and list_file_appendix have not the same size')
        return -1
    list_t_in = []
    list_r_in = []
    list_u_in = []
    list_u = []
    list_ekin = []
    for template_file_appendix in list_template_file_appendix:
        file_list = glob.glob('greens_template%s.hdf5' % template_file_appendix)
        if not file_list:
            return -1
        file = h5py.File(file_list[0], 'r')
        t_in = np.array(file['t'][...], dtype=np.float64)
        log_p = np.array(file['log_p'][...], dtype=np.float64)
        r_in = np.array(file['r'][...], dtype=np.float64)
        u_in = np.array(file['u'][...], dtype=np.float64)
        log_p = log_p[1:]  # cut of first point
        u_in = u_in[:, 1:, :]

        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

        u_in, t_in, r_in = preprocess_u_for_interpolation(u_in, t_in, r_in)
        list_t_in += [t_in]
        list_r_in += [r_in]
        list_u_in += [u_in]
        list_u += [np.empty((sample_size, u_in.shape[2]), dtype=np.float64)]
        list_ekin += [ekin]

    source_count = int(3 * t_end / 1e-4)
    t_start = 1e-3
    r_max = R
    source_count = int(3 * t_end / 1e-4 * r_max ** 2 / 15 ** 2)  # Same as Minh
    print('N_s = %.2e' % source_count, source_count)

    print('Start calculating sample_spectrum%s.hdf5 and %i more (%i samples).' % (
        list_file_appendix[0], len(list_file_appendix) - 1, sample_size), flush=True)
    timer_start = time.perf_counter()

    for i in range(sample_size):
        t0, r0 = source_distribution(source_count, t_start, t_end, r_max)
        for j in range(len(list_u)):
            u_interpol = interpolate_template(list_u_in[j], list_t_in[j], list_r_in[j], t0, r0)
            list_u[j][i] = np.sum(u_interpol, 0)
        time_now = time.perf_counter()
        print('Calculated %i/%i samples in %.2fs / %.2fs.' % (
            i + 1, sample_size, time_now - timer_start, (time_now - timer_start) * sample_size / (i + 1)),
              flush=True, end='\r')

    """Save the results as .hdf5 file"""
    for i, file_appendix in enumerate(list_file_appendix):
        print('Save file sample_spectrum%s.hdf5...' % file_appendix, flush=True, end='\r')
        file = h5py.File('sample_spectrum%s.hdf5' % file_appendix, 'w')
        file.create_dataset('ekin', data=list_ekin[i])
        file.create_dataset('u', data=list_u[i])
    timer_end = time.perf_counter()
    print('Finished calculating sample_spectrum%s.hdf5 in %f seconds.     ' % (file_appendix, timer_end - timer_start),
          flush=True)
    return 0


def stochastic_analysis(file_appendix='', p_quantile=[0.68, 0.95], rescale=False, do_plot_spectrum=True,
                        do_plot_ionisation=True, do_plot_mean=False, do_plot_mean_integrated=False,
                        file_appendix_2=None):
    """
    Calculate median and quantile of spectrum and ionisation rate for a sample of source distributions.

    :param file_appendix: file identifier for the sample,
     the file 'sample_spectrum'+file_appendix_'.hdf5' is used if it exists.
    :param p_quantile: the quantiles to show
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param do_plot_spectrum: if True the plot 'CRDiffStochasticSpectrum' is created
    :param do_plot_ionisation: if True the plot 'CRDiffStochasticIonisationRate' is created
    :param do_plot_mean: if True the mean is also plotted
    :param do_plot_mean_integrated: if True the integrated mean is also plotted
    :param file_appendix_2: file identifier for a second sample
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('sample_spectrum%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    ekin = file['ekin'][...]
    u = file['u'][...]

    sample_size = u.shape[0]

    use_analytic = False  # The second template is called analytic because of history of the code
    if file_appendix_2 is not None:
        use_analytic = True
        file_list = glob.glob('sample_spectrum%s.hdf5' % file_appendix_2)
        if not file_list:
            use_analytic = False
    if use_analytic:
        file = h5py.File(file_list[0], 'r')
        ekin_analytic = file['ekin'][...]
        u_analytic = file['u'][...]

    p = np.sqrt((ekin + mass_p) ** 2 - mass_p ** 2)
    beta = np.sqrt(1. - 1. / (1. + ekin / mass_p) ** 2)
    if use_analytic:
        p_analytic = np.sqrt((ekin_analytic + mass_p) ** 2 - mass_p ** 2)
        beta_analytic = np.sqrt(1. - 1. / (1. + ekin_analytic / mass_p) ** 2)

    """Calculate j0: j0 = beta / (4 pi) * sqrt(1 + (m c / p)^2) u"""
    j0 = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * u * 1.02e-50  # intenesity in (m^2 s sr MeV)^-1
    if use_analytic:
        j0_analytic = beta_analytic / (4. * np.pi) * np.sqrt(1. + (mass_p / p_analytic) ** 2) * u_analytic * 1.02e-50
    j0_phan = intensity_phan(ekin)  # intensity by phan et al

    if False:
        """Calculate Escape time"""
        index0 = 6
        p02 = np.sqrt((ekin[index0] + mass_p) ** 2 - mass_p ** 2)
        print('p0\t', p02)
        print('E0\t', ekin[index0])
        u02 = np.mean(u[:, index0])
        j02 = np.mean(j0[:, index0])
        print('j0\t', j02)
        print('u0\t', u02)
        A = 3e4 / (np.pi * (R * 1.5) ** 2 * 2 * h)
        print('A\t', A)
        b02 = b0 * (p02 / p0) ** n_analytic
        q02 = q0 * (p02 / p0) ** (- gamma)
        print('b0\t', b02)
        print('q0\t', q02)
        fac = u02 * b02 / (A * q02 * p02)
        print('fac\t', fac)
        print('fac2\t', A * q02 * p02 / b02 * 1 / (gamma - 1))
        # fac = 1
        print('parameter', fac * (gamma - 1) - 1, 1 / (1 - gamma))
        p1 = p02 * np.power(fac * (gamma - 1) - 1, 1 / (1 - gamma))
        print('p1\t', p1)

    if rescale:
        j0_median = np.median(j0, axis=0)
        scaling_factor = calculate_scaling_factor(ekin, j0_median)
        print('scaling factor: %.4f, difference factor: %.2f' % (scaling_factor, 1 / scaling_factor))
        j0 *= scaling_factor
        u *= scaling_factor
        if use_analytic:
            j0_median_analytic = np.median(j0_analytic, axis=0)
            scaling_factor = calculate_scaling_factor(ekin_analytic, j0_median_analytic)
            print(scaling_factor)
            j0_analytic *= scaling_factor
            u_analytic *= scaling_factor

    j0_median = np.median(j0, axis=0)
    j0_mean = np.mean(j0, axis=0)
    p_quantile = np.array(p_quantile)
    # j0_quantile_lower = np.quantile(j0, (1. - p_quantile) / 2., axis=0)
    # j0_quantile_upper = np.quantile(j0, (1. + p_quantile) / 2., axis=0)
    j0_quantile_lower = np.quantile(j0, (1. - p_quantile) / 2., axis=0, interpolation='linear')
    j0_quantile_upper = np.quantile(j0, (1. + p_quantile) / 2., axis=0, interpolation='linear')
    if use_analytic:
        j0_median_analytic = np.median(j0_analytic, axis=0)
        j0_mean_analytic = np.mean(j0_analytic, axis=0)
        j0_quantile_lower_analytic = np.quantile(j0_analytic, (1. - p_quantile) / 2., axis=0)
        j0_quantile_upper_analytic = np.quantile(j0_analytic, (1. + p_quantile) / 2., axis=0)

    np.savetxt('Quantiles95%s.csv' % file_appendix,
               np.transpose([ekin, j0_mean, j0_quantile_lower[0], j0_quantile_upper[0]]),
               header='Ekin [MeV], mean (sample), lower, upper [(m^2 s sr MeV)^-1)]')

    if do_plot_spectrum:
        fig = plt.figure()
        label = ''
        plt.plot(ekin, j0_median, '-', label='Median')
        if do_plot_mean:
            plt.plot(ekin, j0_mean, '-', label='Mean - sample')
        if do_plot_mean_integrated:
            j0_mean_integrated = spectrum_mean(file_appendix, rescale=rescale, do_plot=False)
            if type(j0_mean_integrated) == np.ndarray:
                plt.plot(ekin, j0_mean_integrated, label='Mean - integrated')
        for i, p in enumerate(p_quantile):
            plt.fill_between(ekin, j0_quantile_lower[i], j0_quantile_upper[i],
                             label='%.f%%' % (p * 100), alpha=0.3)
        if use_analytic:
            label = file_appendix_2
            plt.plot(ekin_analytic, j0_median_analytic, '--', label='Median' + label, color='red')
            if do_plot_mean:
                plt.plot(ekin_analytic, j0_mean_analytic, '--', label='Mean' + label, color='green')
            if do_plot_mean_integrated:
                j0_mean_integrated = spectrum_mean(file_appendix_2, rescale=rescale, do_plot=False)
                if type(j0_mean_integrated) == np.ndarray:
                    plt.plot(ekin, j0_mean_integrated, '--', label='Mean ' + label + ' - integrated')
            for i, p in enumerate(p_quantile):
                plt.plot(ekin_analytic, j0_quantile_lower_analytic[i], '-', color='black',
                         label='%.f%%' % (p * 100) + label)
                plt.plot(ekin_analytic, j0_quantile_upper_analytic[i], '-', color='black')
                # plt.fill_between(ekin_analytic, j0_quantile_lower_analytic[i], j0_quantile_upper_analytic[i],
                #                  label='%.f%%' % (p * 100) + label, alpha=0.3, linestyle='--')
        plt.plot(ekin, j0_phan, label='Fitted')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffStochasticSpectrum%s.pdf' % file_appendix)

    log_ekin = np.log(ekin)  # log(E_kin / MeV)
    if use_analytic:
        log_ekin_analytic = np.log(ekin_analytic)
    nH2_array = np.power(10, np.linspace(20, 23, 10))
    ionisation_rate_phan = calculate_ionisation_rate(u, log_ekin, n_ion, nH2_array, use_phan=True)

    ionisation_rate = np.empty((sample_size, nH2_array.size), dtype=np.float64)
    if use_analytic:
        ionisation_rate_analytic = np.empty((sample_size, nH2_array.size), dtype=np.float64)

    for i in range(sample_size):
        ionisation_rate[i] = calculate_ionisation_rate(u[i], log_ekin, n_ion, nH2_array)
        if use_analytic:
            ionisation_rate_analytic[i] = calculate_ionisation_rate(u_analytic[i], log_ekin_analytic, n_ion, nH2_array)

    ionrate_median = np.median(ionisation_rate, axis=0)
    ionrate_quantile_lower = np.quantile(ionisation_rate, (1. - p_quantile) / 2., axis=0)
    ionrate_quantile_upper = np.quantile(ionisation_rate, (1. + p_quantile) / 2., axis=0)
    if use_analytic:
        ionrate_median_analytic = np.median(ionisation_rate_analytic, axis=0)
        ionrate_quantile_lower_analytic = np.quantile(ionisation_rate_analytic, (1. - p_quantile) / 2., axis=0)
        ionrate_quantile_upper_analytic = np.quantile(ionisation_rate_analytic, (1. + p_quantile) / 2., axis=0)

    if do_plot_ionisation:
        fig = plt.figure()
        plt.plot(nH2_array, ionrate_median, label='Median')
        for i, p in enumerate(p_quantile):
            plt.fill_between(nH2_array, ionrate_quantile_lower[i], ionrate_quantile_upper[i],
                             label='%.f%%' % (p * 100), alpha=0.3)
        if use_analytic:
            label = file_appendix_2
            plt.plot(nH2_array, ionrate_median_analytic, label='Median' + label)
            for i, p in enumerate(p_quantile):
                plt.fill_between(nH2_array, ionrate_quantile_lower_analytic[i], ionrate_quantile_upper_analytic[i],
                                 label='%.f%%' % (p * 100) + label, alpha=0.3)
        plt.plot(nH2_array, ionisation_rate_phan, label='Fitted')
        plt.xlabel(r'$N_{H_2}$ [$cm^{-2}$]')
        plt.ylabel(r'$\xi (N_{H_2})$ [$s^{-1}$]')
        # plt.title('Ionisation rate')
        plot_ionisation_data(False, False, True)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffStochasticIonisationRate%s.pdf' % file_appendix)
    return 0